package ovirt_test

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlab-runner/common"
	"gitlab.com/gitlab-org/gitlab-runner/helpers"
	"gitlab.com/gitlab-org/gitlab-runner/helpers/ssh"

	ovrt "gitlab.com/gitlab-org/gitlab-runner/helpers/ovirt"
)

const testName = "IntegrationTest"
const ovrtURL = "https://ovirt.gitlab.com"
const ovrtCAFile = "/etc/gitlab-runner/ovirt.pem"
const ovrtUsername = "username@internal"
const ovrtPassword = "password"
const ovrtClusterName = "Cluster"
const ovrtTemplateName = "Template"
const ovrtBaseName = "Base"

var ovrtSSHConfig = &ssh.Config{
	User:     "root",
	Password: "password",
}

func skipIntegrationTests(t *testing.T) bool {
	var ovirt = ovrt.Ovirt{
		Username: ovrtUsername,
		Password: ovrtPassword,
		Timeout:  300,
	}
	ovirt.Client = ovirt.HTTPClient(ovrtCAFile, 5)
	ovirt.URL = ovrtURL
	_, err := ovirt.ID("IntegrationTest")
	if err != nil {
		return true
	}
	return false
}

func TestOvirtExecutorRegistered(t *testing.T) {
	executors := common.GetExecutors()
	assert.Contains(t, executors, "ovirt")
}

func TestOvirtCreateExecutor(t *testing.T) {
	executor := common.NewExecutor("ovirt")
	assert.NotNil(t, executor)
}

func TestOvirtSuccessRun(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	successfulBuild, err := common.GetRemoteSuccessfulBuild()
	assert.NoError(t, err)
	build := &common.Build{
		JobResponse: successfulBuild,
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err = build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	assert.NoError(t, err, "Make sure that the credentials, cluster, and template are correct")
}

func TestOvirtBuildFail(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	failedBuild, err := common.GetRemoteFailedBuild()
	assert.NoError(t, err)
	build := &common.Build{
		JobResponse: failedBuild,
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err = build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err, "error")
	assert.IsType(t, err, &common.BuildError{})
	assert.Contains(t, err.Error(), "Process exited with: 1")
}

func TestOvirtMissingSSHCredentials(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	build := &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
			},
		},
	}

	err := build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "SSH Config - Missing SSH configuration")
}

func TestOvirtDefinedSSHHost(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	build := &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: &ssh.Config{
					Host:     "host",
					User:     "root",
					Password: "password",
				},
			},
		},
	}

	err := build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "SSH Config - Setting the `host` is not allowed for this runner")
}

func TestOvirtMissingURL(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	build := &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err := build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Missing server 'url'")
}

func TestOvirtMissingCredentials(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	build := &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err := build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Missing 'username' & 'password'")

	build = &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err = build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Missing 'username' & 'password'")

	build = &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err = build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Missing 'username' & 'password'")
}

func TestOvirtMissingCluster(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	build := &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err := build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Missing 'cluster_name'")
}

func TestOvirtMissingImage(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	build := &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:         ovrtURL,
					CAFile:      ovrtCAFile,
					Username:    ovrtUsername,
					Password:    ovrtPassword,
					ClusterName: ovrtClusterName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err := build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Missing 'base_name' XOR 'template_name'")

	build = &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
					BaseName:     ovrtBaseName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err = build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Missing 'base_name' XOR 'template_name'")
}

func TestOvirtMissingLimit(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	build := &common.Build{
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:         ovrtURL,
					CAFile:      ovrtCAFile,
					Username:    ovrtUsername,
					Password:    ovrtPassword,
					ClusterName: ovrtClusterName,
					BaseName:    ovrtBaseName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	err := build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	require.Error(t, err)
	assert.Contains(t, err.Error(), "oVirt Config - Error: When not using templates 'limit' must be set to 1 due to https://bugzilla.redhat.com/show_bug.cgi?id=1176806#c16")
}

func TestOvirtBuildAbort(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	longRunningBuild, err := common.GetRemoteLongRunningBuild()
	assert.NoError(t, err)
	build := &common.Build{
		JobResponse: longRunningBuild,
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
		SystemInterrupt: make(chan os.Signal, 1),
	}

	abortTimer := time.AfterFunc(time.Second, func() {
		t.Log("Interrupt")
		build.SystemInterrupt <- os.Interrupt
	})
	defer abortTimer.Stop()

	timeoutTimer := time.AfterFunc(time.Minute*5, func() {
		t.Log("Timedout")
		t.FailNow()
	})
	defer timeoutTimer.Stop()

	err = build.Run(&common.Config{}, &common.Trace{Writer: os.Stdout})
	assert.EqualError(t, err, "aborted: interrupt")
}

func TestOvirtBuildCancel(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	longRunningBuild, err := common.GetRemoteLongRunningBuild()
	assert.NoError(t, err)
	build := &common.Build{
		JobResponse: longRunningBuild,
		Runner: &common.RunnerConfig{
			Name: testName,
			RunnerSettings: common.RunnerSettings{
				Executor: "ovirt",
				Ovirt: &common.OvirtConfig{
					URL:          ovrtURL,
					CAFile:       ovrtCAFile,
					Username:     ovrtUsername,
					Password:     ovrtPassword,
					ClusterName:  ovrtClusterName,
					TemplateName: ovrtTemplateName,
				},
				SSH: ovrtSSHConfig,
			},
		},
	}

	trace := &common.Trace{Writer: os.Stdout}

	abortTimer := time.AfterFunc(time.Second, func() {
		t.Log("Interrupt")
		trace.CancelFunc()
	})
	defer abortTimer.Stop()

	timeoutTimer := time.AfterFunc(time.Minute*5, func() {
		t.Log("Timedout")
		t.FailNow()
	})
	defer timeoutTimer.Stop()

	err = build.Run(&common.Config{}, trace)
	assert.IsType(t, err, &common.BuildError{})
	assert.EqualError(t, err, "canceled")
}
