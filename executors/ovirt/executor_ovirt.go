package ovirt

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/gitlab-org/gitlab-runner/common"
	"gitlab.com/gitlab-org/gitlab-runner/executors"
	"gitlab.com/gitlab-org/gitlab-runner/helpers/ssh"

	ovrt "gitlab.com/gitlab-org/gitlab-runner/helpers/ovirt"
)

type executor struct {
	executors.AbstractExecutor
	sshCommand ssh.Client
	sshConfig  ssh.Config
	ovirt      ovrt.Ovirt
	vmID       string
	vmName     string
}

func (s *executor) getMachineIP() (string, error) {
	// Get Interface
	var nic ovrt.NIC
	nicID, err := s.ovirt.NICID(s.vmID, s.Config.Ovirt.Interface)
	if err != nil {
		return "", err
	}
	if nicID == "" {
		return "", errors.New("Failed to find specfied interface: " + s.Config.Ovirt.Interface)
	}
	err = s.ovirt.NIC(&nic, s.vmID, nicID)
	if err != nil {
		return "", err
	}

	// Get IPv4 Address
	version := "v4"
	if s.Config.Ovirt.UseIPv6 {
		version = "v6"
	}
	if nic.ReportedDevices != nil {
		ips := nic.ReportedDevices.ReportedDevice[0].IPs.IP
		for _, ip := range ips {
			if ip.Version == version && ip.Address != "" {
				return ip.Address, nil
			}
		}
	}
	return "", nil
}

func (s *executor) verifyMachine() error {
	if s.Config.Ovirt.UseIPv6 {
		s.Debugln("Getting IPv6 address for interface", s.Config.Ovirt.Interface, "...")
	} else {
		s.Debugln("Getting IPv4 address for interface", s.Config.Ovirt.Interface, "...")
	}

	// Guest additions can be slow, try to obtain an IP for at most a minute
	for i := 0; i < 30; i++ {
		ip, err := s.getMachineIP()
		if err != nil {
			return err
		}
		if ip != "" {
			s.sshConfig.Host = ip
			break
		}
		time.Sleep(2 * time.Second)
	}

	if s.sshConfig.Host == "" {
		return errors.New("Failed to find IP address")
	}

	s.Debugln("Verifying IP via SSH...")
	sshCommand := ssh.Client{
		Config:         s.sshConfig,
		Stdout:         s.Trace,
		Stderr:         s.Trace,
		ConnectRetries: 30,
	}
	err := sshCommand.Connect()
	if err != nil {
		return err
	}
	defer sshCommand.Cleanup()
	err = sshCommand.Run(s.Context, ssh.Command{Command: []string{"exit"}})
	if err != nil {
		return err
	}
	return nil
}

func (s *executor) prePrepare() error {
	if s.Config.SSH == nil {
		return errors.New("SSH Config - Missing SSH configuration")
	}

	if s.Config.SSH.Host != "" {
		return errors.New("SSH Config - Setting the `host` is not allowed for this runner")
	}

	if s.Config.Ovirt == nil {
		return errors.New("oVirt Config - Missing oVirt configuration")
	}

	if s.Config.Ovirt.URL == "" {
		return errors.New("oVirt Config - Missing server 'url'")
	}

	if s.Config.Ovirt.Username == "" || s.Config.Ovirt.Password == "" {
		return errors.New("oVirt Config - Missing 'username' & 'password'")
	}

	if s.Config.Ovirt.ClusterName == "" {
		return errors.New("oVirt Config - Missing 'cluster_name'")
	}

	if !((s.Config.Ovirt.BaseName != "") != (s.Config.Ovirt.TemplateName != "")) {
		return errors.New("oVirt Config - Missing 'base_name' XOR 'template_name'")
	}

	if s.Config.Ovirt.BaseName != "" && s.Config.Limit != 1 {
		return errors.New("oVirt Config - Error: When not using templates 'limit' must be set to 1 due to https://bugzilla.redhat.com/show_bug.cgi?id=1176806#c16")
	}

	// Defaults
	if s.Config.Ovirt.Interface == "" {
		s.Config.Ovirt.Interface = "virtio"
	}

	if s.Config.Ovirt.Timeout == 0 {
		s.Config.Ovirt.Timeout = 300
	}

	// Copy the SSH config to prevent concurrency errors
	s.sshConfig = *s.Config.SSH

	// Setup Ovirt Helper
	client := s.ovirt.HTTPClient(s.Config.Ovirt.CAFile, s.Config.Ovirt.Timeout)
	s.ovirt = ovrt.Ovirt{
		Client:   client,
		Password: s.Config.Ovirt.Password,
		Timeout:  s.Config.Ovirt.Timeout,
		URL:      s.Config.Ovirt.URL,
		Username: s.Config.Ovirt.Username,
	}

	return nil
}

func (s *executor) recoverMachine() error {
	// Make sure we are in a workable state
	err := s.ovirt.WaitForStatus(s.vmID, []string{"up", "down"}, s.Config.Ovirt.Timeout)
	if err != nil {
		return err
	}

	var snapshotID string
	snapshotID, err = s.ovirt.SnapshotID(s.vmID, "Started")
	if err != nil {
		return err
	}
	if snapshotID != "" {
		s.Debugln("Restoring VM from Snapshot...")
		err = s.ovirt.RestoreSnapshot(s.vmID, snapshotID)
	}
	if snapshotID == "" || err != nil {
		s.ovirt.Shutdown(s.vmID)
		s.ovirt.Delete(s.vmID)
		s.vmID, err = s.ovirt.ID(s.vmName)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *executor) createMachine() error {
	var err error
	if s.Config.Ovirt.TemplateName != "" {
		s.Debugln("Creating new VM from Template...")
		s.vmID, err = s.ovirt.CreateFromTemplate(s.vmName, s.Config.Ovirt.TemplateName, s.Config.Ovirt.ClusterName)
		if err != nil {
			return err
		}
	} else {
		s.vmID, err = s.ovirt.ID(s.Config.Ovirt.BaseName)
		if err != nil {
			return err
		}
		if s.vmID == "" {
			return errors.New("Cannot find VM with name " + s.Config.Ovirt.BaseName)
		}

		if s.Config.Ovirt.BaseSnapshotName != "" {
			s.Debugln("Creating new VM from BaseSnapshotName Snapshot...")
			s.vmID, err = s.ovirt.CreateFromSnapshot(s.vmName, s.vmID, s.Config.Ovirt.BaseSnapshotName, s.Config.Ovirt.ClusterName)
			if err != nil {
				return err
			}
		} else {
			// NOTE: We cannot create a VM from "Active VM" this will cause an
			// Engine error, hence we must create a snapshot and do some pretend
			// mutexing...
			s.Debugln("Creating new VM from Default Snapshot...")
			snapshotID, err := s.ovirt.SnapshotID(s.vmID, "Default")
			if err != nil {
				return err
			}
			if snapshotID == "" {
				s.Debugln("Creating 'Default' snapshot for VM...")
				snapshotID, err = s.ovirt.CreateSnapshot(s.vmID, "Default")
				if err != nil && !strings.Contains(err.Error(), "409") {
					return err
				}
			}
			err = s.ovirt.WaitForSnapshotStatus(s.vmID, snapshotID, "ok", s.Config.Ovirt.Timeout)
			if err != nil {
				return err
			}
			s.vmID, err = s.ovirt.CreateFromSnapshot(s.vmName, s.vmID, "Default", s.Config.Ovirt.ClusterName)
			if err != nil {
				return err
			}
		}
	}

	s.Debugln("Spinning up VM...")
	err = s.ovirt.Start(s.vmID)
	if err != nil {
		return err
	}

	if s.Config.Ovirt.Preserve {
		s.Debugln("Creating Snapshot for VM")
		_, err = s.ovirt.CreateSnapshot(s.vmID, "Started")
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *executor) spinMachine() error {
	status, err := s.ovirt.Status(s.vmID)
	if err != nil {
		return err
	}
	if status != "up" {
		s.Debugln("Spinning up VM...")
		err = s.ovirt.WaitForStatus(s.vmID, []string{"down"}, s.Config.Ovirt.Timeout)
		if err != nil {
			return err
		}
		err = s.ovirt.Start(s.vmID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *executor) Prepare(options common.ExecutorPrepareOptions) error {
	err := s.AbstractExecutor.Prepare(options)
	if err != nil {
		return err
	}

	// Validate config variables, set defaults and configure
	err = s.prePrepare()
	if err != nil {
		return err
	}

	// Generate VM name
	s.Debugln("Generating VM name...")
	if s.Config.Ovirt.Preserve {
		s.vmName = fmt.Sprintf("%s-runner-%s-concurrent-%d",
			s.Config.Name,
			s.Build.Runner.ShortDescription(),
			s.Build.RunnerID)
	} else {
		s.vmName = s.Config.Name + "-" + s.Build.ProjectUniqueName()
	}

	// Check if VM exists
	s.Debugln("Checking if VM exists...")
	s.vmID, err = s.ovirt.ID(s.vmName)
	if err != nil {
		return err
	}

	// Try to recover the VM if possible
	if s.vmID != "" {
		s.recoverMachine()
		if err != nil {
			return err
		}
	}

	// Create the new VM if recovery was not possible
	if s.vmID == "" {
		s.createMachine()
		if err != nil {
			return err
		}
	}

	// Spin VM if it is not 'up'
	s.Debugln("Spinning VM...")
	err = s.spinMachine()
	if err != nil {
		return err
	}

	s.Debugln("Waiting for SSH to become responsive...")
	err = s.verifyMachine()
	if err != nil {
		return err
	}

	s.Debugln("Connecting to SSH server...")
	s.sshCommand = ssh.Client{
		Config: s.sshConfig,
		Stdout: s.Trace,
		Stderr: s.Trace,
	}
	err = s.sshCommand.Connect()
	if err != nil {
		return err
	}
	return nil
}

func (s *executor) Run(cmd common.ExecutorCommand) error {
	err := s.sshCommand.Run(cmd.Context, ssh.Command{
		Environment: s.BuildShell.Environment,
		Command:     s.BuildShell.GetCommandWithArguments(),
		Stdin:       cmd.Script,
	})
	if _, ok := err.(*ssh.ExitError); ok {
		err = &common.BuildError{Inner: err}
	}
	return err
}

func (s *executor) Cleanup() {
	s.sshCommand.Cleanup()

	if s.vmID != "" {
		s.Debugln("Spinning down VM...")
		s.ovirt.Shutdown(s.vmID)
		if !s.Config.Ovirt.Preserve {
			s.Debugln("Removing VM...")
			s.ovirt.Delete(s.vmID)
		}
	}

	s.AbstractExecutor.Cleanup()
}

func init() {
	options := executors.ExecutorOptions{
		DefaultBuildsDir: "builds",
		SharedBuildsDir:  false,
		Shell: common.ShellScriptInfo{
			Shell:         "bash",
			Type:          common.LoginShell,
			RunnerCommand: "gitlab-runner",
		},
		ShowHostname: true,
	}

	creator := func() common.Executor {
		return &executor{
			AbstractExecutor: executors.AbstractExecutor{
				ExecutorOptions: options,
			},
		}
	}

	featuresUpdater := func(features *common.FeaturesInfo) {
		features.Variables = true
	}

	common.RegisterExecutor("ovirt", executors.DefaultExecutorProvider{
		Creator:         creator,
		FeaturesUpdater: featuresUpdater,
	})
}
