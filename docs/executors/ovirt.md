# oVirt

>**Note:**
The oVirt executor works the same as the VirtualBox executor. The
caching feature is currently not supported.

oVirt is a free, open-source virtualization management platform. This executor
supports all systems that can be run on VirtualBox. The only requirement is
that the virtual machine exposes its SSH server and provide a bash-compatible
shell.

## Overview

The project's source code is checked out to: `~/builds/<namespace>/<project-name>`.

Where:

- `<namespace>` is the namespace where the project is stored on GitLab
- `<project-name>` is the name of the project as it is stored on GitLab

To overwrite the `~/builds` directory, specify the `builds_dir` option under
the `[[runners]]` section in [`config.toml`](../configuration/advanced-configuration.md).

## Create a Virtual Machine

1. Create a new Template or Virtual Machine, depending on your preference...
1. If on Windows, see [Checklist for Windows VMs](#checklist-for-windows-vms)
1. Ensure that OpenSSH server is installed
1. Ensure that all other dependencies required by the build are installed (or download them at runtime)
1. Leave the Virtual Machine in a bootable or snapshot-able state

## Create a New Runner

1. Install GitLab Runner where you see fit
1. Register a new GitLab Runner with `gitlab-runner register`
1. Select the `ovirt` executor
1. Enter the following oVirt options (or see `--help`):
  - The URL (`url`) to the oVirt Engine
  - The oVirt Engine `username` & `password` with sufficient permissions
  - The Cluster (`cluster_name`) to use
  - The Template (`template_name`) to spin from, or the VM (`base_name`) with Snapshot (`base_snapshot_name`) (Optional)
1. Enter the following SSH options (or see `--help`)
  - The SSH `user` and `password` or path to `identity_file` of the virtual machine

## How It Works

When a new build is started:

### Default

1. A unique name for the VM is generated: `<name>-<project_unique_name>`
1. The Runner creates the VM and spins it
1. The Runner waits for the SSH server to become accessible
1. The Runner connects to the virtual machine and executes a build
1. The Runner shuts down and deletes the VM

### With `preserve`

1. A unique name for the VM is generated: `<name>-runner-<short-token>-concurrent-<id>`
1. The Runner does the following:
  - On first run: creates the VM, spins it, and snapshots it
  - On following runs: restores the VM and spins it
1. The Runner waits for the SSH server to become accessible
1. The Runner connects to the virtual machine and executes a build
1. The Runner stops or shutdowns the virtual machine

## Example Runner

Below is a sample configuration for an oVirt based executor.

```
[[runners]]
  name = "CentOS-7"
  url = "https://gitlab.domain.tld/ci"
  token = "XXX"
  tls-ca-file = "/etc/gitlab-runner/gitlab.pem"
  executor = "ovirt"
  [runners.ssh]
    user = "root"
    password = "XXX"
    port = "22"
  [runners.cache]
  [runners.ovirt]
    url = "https://engine.domain.tld"
    ca_file = "/etc/gitlab-runner/ovirt.pem"
    username = "username@domain"
    password = "XXX"
    cluster_name = "Cluster-01"
    template_name = "CentOS-7-Template"
```

Available options for oVirt:

```
url                 - oVirt Server URL
ca_file             - File containing the certificates to verify the peer when using HTTPS
username            - oVirt authentication username, i.e. user@domain
password            - oVirt authentication password
base_name           - Name of the virtual machine to clone
base_snapshot_name  - Name of a specific VM snapshot to clone
cluster_name        - Cluster for the creation of a virtual machine
template_name       - Template for the creation of a virtual machine
interface           - The interface to SSH over (default: virtio)
preserve,omitempty  - Preserve VM after creation (default: false)
timeout             - Timeout value for communicating with the API, i.e. Waiting for VM power up (default 300s)
use_ipv6            - Use the IPv6 protocol instead of the IPv4 protocol (default: false)
```

## Checklist for Windows VMs
* install [Cygwin]
* install sshd and git from cygwin (do not use *Git For Windows*, you will get lots of path issues!)
* configure sshd and set it up as a service (see [cygwin wiki](http://cygwin.wikia.com/wiki/Sshd))
* create a rule for the windows firewall to allow incoming TCP traffic on port 22
* add the gitlab server(s) to `~/.ssh/known_hosts`
* to convert paths between cygwin and windows, use the `cygpath` utility which is documented [here](http://cygwin.wikia.com/wiki/Cygpath_utility)

[cygwin]: https://cygwin.com/
