package ovirt

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

// Ovirt configuration structure
type Ovirt struct {
	Client   *http.Client
	Password string
	Timeout  int
	URL      string
	Username string
}

func (s *Ovirt) sendRequest(method string, command string, data string, status int) (*http.Response, error) {
	if s.Client == nil {
		return nil, errors.New("No client available to send requests")
	}

	d := &bytes.Buffer{}
	if data != "" {
		d = bytes.NewBuffer([]byte(data))
	}

	req, err := http.NewRequest(method, s.URL+"/ovirt-engine/api/"+command, d)
	req.SetBasicAuth(s.Username, s.Password)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Version", "4")
	if d != nil {
		req.Header.Add("Content-Type", "application/json")
	}

	resp, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != status {
		body, _ := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		return nil, errors.New(resp.Status + " for " + req.URL.String() + ": " + string(body))
	}

	return resp, nil
}

// HTTPClient creates an oVirt HTTP client
func (s *Ovirt) HTTPClient(caFile string, timeout int) *http.Client {
	tr := &http.Transport{}
	if caFile != "" {
		caCert, err := ioutil.ReadFile(caFile)
		if err != nil {
			return nil
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		tr = &http.Transport{
			TLSClientConfig: &tls.Config{RootCAs: caCertPool},
		}
	}

	client := &http.Client{
		Timeout:   time.Second * time.Duration(timeout),
		Transport: tr,
	}

	return client
}

// ID get the ID for a given VM name.
// It returns the ID and any error.
func (s *Ovirt) ID(vmName string) (string, error) {
	type RespJSON struct {
		VM []VM `json:"vm"`
	}
	var respJSON RespJSON
	resp, err := s.sendRequest("GET", "vms?search="+vmName, "", 200)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return "", err
	}
	for _, element := range respJSON.VM {
		if element.Name == vmName {
			return element.ID, nil
		}
	}
	return "", nil
}

// VM gets the information of a VM based on the given ID.
// It returns a populated VM structure and any error.
func (s *Ovirt) VM(vm *VM, vmID string) error {
	resp, err := s.sendRequest("GET", "vms/"+vmID, "", 200)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(vm)
	if err != nil {
		return err
	}
	return nil
}

// NICID gets the NIC ID for a given interface and VM.
// It returns the NIC ID and any error.
func (s *Ovirt) NICID(vmID string, iface string) (string, error) {
	type RespJSON struct {
		NIC []NIC `json:"nic"`
	}
	var respJSON RespJSON
	resp, err := s.sendRequest("GET", "vms/"+vmID+"/nics", "", 200)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return "", err
	}
	for _, element := range respJSON.NIC {
		if element.Interface == iface {
			return element.ID, nil
		}
	}
	return "", nil
}

// NIC gets the information of a NIC based on the given ID.
// It returns a populated NIC structure and any error.
func (s *Ovirt) NIC(nic *NIC, vmID string, nicID string) error {
	resp, err := s.sendRequest("GET", "vms/"+vmID+"/nics/"+nicID, "", 200)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(nic)
	if err != nil {
		return err
	}
	return nil
}

// SnapshotID gets the Sapshot ID for a given snapshot description and VM.
// It returns the Snapshot ID and any error.
func (s *Ovirt) SnapshotID(vmID string, snapshotDescription string) (string, error) {
	type RespJSON struct {
		Snapshot []Snapshot `json:"snapshot"`
	}
	var respJSON RespJSON
	resp, err := s.sendRequest("GET", "vms/"+vmID+"/snapshots", "", 200)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return "", err
	}
	for _, element := range respJSON.Snapshot {
		if element.Description == snapshotDescription {
			return element.ID, nil
		}
	}
	return "", nil
}

// Snapshot gets the information of a Snapshot based on the given ID.
// It returns a populated Snapshot structure and any error.
func (s *Ovirt) Snapshot(snapshot *Snapshot, vmID string, snapshotID string) error {
	resp, err := s.sendRequest("GET", "vms/"+vmID+"/snapshots/"+snapshotID, "", 200)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(snapshot)
	if err != nil {
		return err
	}
	return nil
}

// Status gets the VM status for a given VM ID.
// It returns the status and any error.
func (s *Ovirt) Status(vmID string) (string, error) {
	var vm VM
	resp, err := s.sendRequest("GET", "vms/"+vmID+"?all_content=true", "", 200)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&vm)
	if err != nil {
		return "", err
	}
	return vm.Status, nil
}

// WaitForStatus waits for a given array of statuses on a VM for a given maximum timeout.
// It returns any error that may have occured.
func (s *Ovirt) WaitForStatus(vmID string, statuses []string, timeout int) error {
	var interval = 2
	for i := 0; i < (timeout / interval); i++ {
		status, err := s.Status(vmID)
		if err != nil {
			return err
		}
		for _, s := range statuses {
			if s == status {
				return nil
			}
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
	return errors.New("Failed to wait for given status")
}

// SnapshotStatus gets the Snapshot status for a given snapshot ID.
// It returns the status and any error.
func (s *Ovirt) SnapshotStatus(vmID string, snapshotID string) (string, error) {
	var snapshot Snapshot
	resp, err := s.sendRequest("GET", "vms/"+vmID+"/snapshots/"+snapshotID, "", 200)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&snapshot)
	if err != nil {
		return "", err
	}
	return snapshot.SnapshotStatus, nil
}

// WaitForSnapshotStatus waits for a given status on a snapshot for a given maximum timeout.
// It returns any error that may have occured.
func (s *Ovirt) WaitForSnapshotStatus(vmID string, snapshotID string, status string, timeout int) error {
	var interval = 2
	for i := 0; i < (timeout / interval); i++ {
		s, err := s.SnapshotStatus(vmID, snapshotID)
		if err != nil {
			return err
		}
		if s == status {
			return nil
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
	return errors.New("Failed to wait for given status")
}

// CreateFromTemplate creates a VM from a specified template.
// It returns the newly created VM's ID and any error.
func (s *Ovirt) CreateFromTemplate(name string, template string, cluster string) (string, error) {
	var vm VM
	jsonData := `{"name": "` + name + `", "template": {"name": "` + template + `"}, "cluster": {"name": "` + cluster + `"}}`
	resp, err := s.sendRequest("POST", "vms", jsonData, 202)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&vm)
	if err != nil {
		return "", err
	}
	err = s.WaitForStatus(vm.ID, []string{"down"}, s.Timeout)
	if err != nil {
		return "", err
	}
	return vm.ID, nil
}

// CreateFromSnapshot creates a VM from a specified snapshot.
// It returns the newly created VM's ID and any error.
func (s *Ovirt) CreateFromSnapshot(name string, vmID string, snapshot string, cluster string) (string, error) {
	var vm VM
	var sID string

	// Get snapshot ID
	sID, err := s.SnapshotID(vmID, snapshot)
	if err != nil {
		return "", err
	}

	if sID == "" {
		return "", errors.New("Cannot create VM from Snapshot: Snapshot not found")
	}

	// TODO: Can't Clone as Thin/Dependent: https://bugzilla.redhat.com/show_bug.cgi?id=1176806#c16

	// Create using snapshot ID
	jsonData := `{"name": "` + name + `", "snapshots": {"snapshot": [{"id": "` + sID + `"}]}, "cluster": {"name": "` + cluster + `"}}`
	resp, err := s.sendRequest("POST", "vms", jsonData, 202)
	if err != nil {
		return "", err
	}
	err = json.NewDecoder(resp.Body).Decode(&vm)
	if err != nil {
		return "", err
	}
	err = s.WaitForStatus(vm.ID, []string{"up", "down"}, s.Timeout)
	if err != nil {
		return "", err
	}
	return vm.ID, nil
}

// Delete removes the VM for a given VM ID.
// It returns any error.
func (s *Ovirt) Delete(vmID string) error {
	type RespJSON struct {
		Job    Link   `json:"job"`
		Status string `json:"status"`
	}
	var respJSON RespJSON
	jsonData := `{"async": "false", "detach_only": "false", "force": "true"}`
	resp, err := s.sendRequest("DELETE", "vms/"+vmID, jsonData, 200)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return err
	}
	if respJSON.Status != "complete" {
		return errors.New("VM delete failed with status - " + respJSON.Status)
	}
	return nil
}

// Start powers up a VM for a given VM ID.
// It returns any error.
func (s *Ovirt) Start(vmID string) error {
	type RespJSON struct {
		Async  string `json:"async"`
		Job    Link   `json:"job"`
		Status string `json:"status"`
		VM     VM     `json:"vm"`
	}
	var respJSON RespJSON
	jsonData := `{"async": "false", "filter": "true"}`
	resp, err := s.sendRequest("POST", "vms/"+vmID+"/start", jsonData, 200)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return err
	}
	if respJSON.Status != "complete" {
		return errors.New("VM shutdown failed with status - " + respJSON.Status)
	}
	// NOTE: The above status does not confirm a successful start up
	err = s.WaitForStatus(vmID, []string{"up"}, s.Timeout)
	if err != nil {
		return err
	}
	return nil
}

// Shutdown powers down a VM for a given VM ID.
// It returns any error.
func (s *Ovirt) Shutdown(vmID string) error {
	type RespJSON struct {
		Async  string `json:"async"`
		Job    Link   `json:"job"`
		Status string `json:"status"`
		VM     VM     `json:"vm"`
	}
	var respJSON RespJSON
	jsonData := `{"async": "false"}`
	resp, err := s.sendRequest("POST", "vms/"+vmID+"/shutdown", jsonData, 200)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return err
	}
	if respJSON.Status != "complete" {
		return errors.New("VM shutdown failed with status - " + respJSON.Status)
	}
	// NOTE: The above status does not confirm a successful shutdown
	err = s.WaitForStatus(vmID, []string{"down"}, s.Timeout)
	if err != nil {
		return err
	}
	return nil
}

// CreateSnapshot creates a snapshot for a given VM.
// It returns the Snapshot ID of the newly created snapshot and any error.
// NOTE: This seems to shutdown the VM, Yay...
func (s *Ovirt) CreateSnapshot(vmID string, snapshotDescription string) (string, error) {
	var snapshot Snapshot
	jsonData := `{"description": "` + snapshotDescription + `"}`
	resp, err := s.sendRequest("POST", "vms/"+vmID+"/snapshots", jsonData, 202)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&snapshot)
	if err != nil {
		return "", err
	}
	err = s.WaitForSnapshotStatus(vmID, snapshot.ID, "ok", s.Timeout)
	if err != nil {
		return "", err
	}
	return snapshot.ID, nil
}

// RestoreSnapshot restores a VM to a give Snapshot.
// It returns any error.
func (s *Ovirt) RestoreSnapshot(vmID string, snapshotID string) error {
	type RespJSON struct {
		Async    string   `json:"async"`
		Job      Link     `json:"job"`
		Snapshot Snapshot `json:"snapshot"`
		Status   string   `json:"status"`
	}
	var respJSON RespJSON
	jsonData := `{"async": "false"}`
	resp, err := s.sendRequest("POST", "vms/"+vmID+"/snapshots/"+snapshotID+"/restore", jsonData, 200)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return err
	}
	if respJSON.Status == "complete" {
		return nil
	}
	return errors.New("Snapshot restore failed with status - " + respJSON.Status)
}
