package ovirt

// TODO: Some API Integer are int while others are string (yay consistency...),
// will have to trial and error due to API omitempty!
// TODO: Fix possible errors with nested structures, that does not come across
// in the API documentation

// AffinityGroup see: https://engine.domain.tld/ovirt-engine/apidoc/#types/affinity_group
type AffinityGroup struct {
	// Attributes
	Comment     string       `json:"comment"`
	Description string       `json:"description"`
	Enforcing   string       `json:"enforcing"`
	HostsRule   AffinityRule `json:"hosts_rule"`
	ID          string       `json:"id"`
	Name        string       `json:"name"`
	Positive    string       `json:"positive"`
	VMSRule     AffinityRule `json:"vms_rule"`
	// Links
	Cluster *Cluster `json:"cluster"`
	Hosts   []*Host  `json:"hosts"`
	VMs     []*VM    `json:"vms"`
}

// AffinityLabel see: https://engine.domain.tld/ovirt-engine/apidoc/#types/affinity_label
type AffinityLabel struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	ReadOnly    string `json:"read_only"`
	// Links
	Hosts []*Host `json:"hosts"`
	VMs   []*VM   `json:"vms"`
}

// AffinityRule see: https://engine.domain.tld/ovirt-engine/apidoc/#types/affinity_rule
type AffinityRule struct {
	Enabled   string `json:"enabled"`
	Enforcing string `json:"enforcing"`
	Positive  string `json:"positive"`
}

// Agent see: https://engine.domain.tld/ovirt-engine/apidoc/#types/agent
type Agent struct {
	// Attributes
	Address        string   `json:"address"`
	Comment        string   `json:"comment"`
	Concurrent     string   `json:"concurrent"`
	Description    string   `json:"description"`
	EncryptOptions string   `json:"encrypt_options"`
	ID             string   `json:"id"`
	Name           string   `json:"name"`
	Options        []Option `json:"options"`
	Order          string   `json:"order"`
	Password       string   `json:"password"`
	Port           string   `json:"port"`
	Type           string   `json:"type"`
	Username       string   `json:"username"`
	// Links
	Host *Host `json:"host"`
}

// API see: https://engine.domain.tld/ovirt-engine/apidoc/#types/api
type API struct {
	// Attributes
	Link           []Link         `json:"link"`
	ProductInfo    ProductInfo    `json:"product_info,omitempty"`
	SpecialObjects SpecialObjects `json:"special_objects"`
	Summary        APISummary     `json:"summary,omitempty"`
	Time           int            `json:"time"`
}

// APISummary see: https://engine.domain.tld/ovirt-engine/apidoc/#types/api_summary
type APISummary struct {
	// Attributes
	Hosts          APISummaryItem `json:"hosts"`
	StorageDomains APISummaryItem `json:"storage_domains"`
	Users          APISummaryItem `json:"users"`
	VMs            APISummaryItem `json:"vms"`
}

// APISummaryItem see: https://engine.domain.tld/ovirt-engine/apidoc/#types/api_summary_item
type APISummaryItem struct {
	// Attributes
	Active int `json:"active"`
	Total  int `json:"total"`
}

// Application see: https://engine.domain.tld/ovirt-engine/apidoc/#types/application
type Application struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	VM *VM `json:"vm"`
}

// Balance see: https://engine.domain.tld/ovirt-engine/apidoc/#types/balance
type Balance struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	SchedulingPolicy     *SchedulingPolicy     `json:"scheduling_policy"`
	SchedulingPolicyUnit *SchedulingPolicyUnit `json:"scheduling_policy_unit"`
}

// BIOS see: https://engine.domain.tld/ovirt-engine/apidoc/#types/bios
type BIOS struct {
	// Attributes
	BootMenu struct {
		Enabled string `json:"enabled"`
	} `json:"boot_menu"`
}

// Bonding see: https://engine.domain.tld/ovirt-engine/apidoc/#types/bonding
type Bonding struct {
	// Attributes
	ADPartnerMAC MAC       `json:"ad_partner_mac"`
	Options      []Option  `json:"options"`
	Slaves       []HostNIC `json:"slaves"`
}

// CDROM see: https://engine.domain.tld/ovirt-engine/apidoc/#types/cdrom
type CDROM struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	File        File   `json:"file"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	InstanceType *InstanceType `json:"instance_type"`
	Template     *Template     `json:"template"`
	VM           *VM           `json:"vm"`
	VMs          []*VM         `json:"vms"`
}

// Certificate see: https://engine.domain.tld/ovirt-engine/apidoc/#types/certificate
type Certificate struct {
	// Attributes
	Comment      string `json:"comment"`
	Content      string `json:"content"`
	Description  string `json:"description"`
	ID           string `json:"id"`
	Name         string `json:"name"`
	Organization string `json:"organization"`
	Subject      string `json:"subject"`
}

// Cluster see: https://engine.domain.tld/ovirt-engine/apidoc/#types/cluster
type Cluster struct {
	// Attributes
	BallooningEnabled                string           `json:"ballooning_enabled"`
	Comment                          string           `json:"comment"`
	CPU                              CPU              `json:"cpu"`
	CustomSchedulingPolicyProperties []Property       `json:"custom_scheduling_policy_properties"`
	Description                      string           `json:"description"`
	Display                          Display          `json:"display"`
	ErrorHandling                    ErrorHandling    `json:"error_handling"`
	FencingPolicy                    FencingPolicy    `json:"fencing_policy"`
	GlusterService                   string           `json:"gluster_service"`
	GlusterTunedProfile              string           `json:"gluster_tuned_profile"`
	HAReservation                    string           `json:"ha_reservation"`
	ID                               string           `json:"id"`
	KSM                              KSM              `json:"ksm"`
	MaintenanceReasonRequired        string           `json:"maintenance_reason_required"`
	MemoryPolicy                     MemoryPolicy     `json:"memory_policy"`
	Migration                        MigrationOptions `json:"migration"`
	Name                             string           `json:"name"`
	OptionalReason                   string           `json:"optional_reason"`
	RequiredRNGSources               []string         `json:"required_rng_sources"`
	SerialNumber                     SerialNumber     `json:"serial_number"`
	SupportedVersions                []Version        `json:"supported_versions"`
	SwitchType                       string           `json:"switch_type"`
	ThreadsAsCores                   string           `json:"threads_as_cores"`
	TrustedService                   string           `json:"trusted_service"`
	TunnelMigration                  string           `json:"tunnel_migration"`
	Version                          Version          `json:"version"`
	VirtService                      string           `json:"virt_service"`
	// Link
	AffinityGroups    []*AffinityGroup  `json:"affinity_groups"`
	CPUProfiles       []*CPUProfile     `json:"cpu_profiles"`
	DataCenter        *DataCenter       `json:"data_center"`
	GlusterHooks      []*GlusterHook    `json:"gluster_hooks"`
	GlusterVolumes    []*GlusterVolume  `json:"gluster_volumes"`
	MACPool           *MACPool          `json:"mac_pool"`
	ManagementNetwork *Network          `json:"management_network"`
	NetworkFilters    []*NetworkFilter  `json:"network_filters"`
	Networks          []*Network        `json:"networks"`
	Permissions       []*Permission     `json:"permissions"`
	SchedulingPolicy  *SchedulingPolicy `json:"scheduling_policy"`
}

// Console see: https://engine.domain.tld/ovirt-engine/apidoc/#types/console
type Console struct {
	// Attributes
	Enabled string `json:"enabled"`
}

// CPU see: https://engine.domain.tld/ovirt-engine/apidoc/#types/cpu
type CPU struct {
	// Attributes
	Architecture string `json:"architecture"`
	Cores        []struct {
		Index  string `json:"index"`
		Socket string `json:"socket"`
	} `json:"cores"`
	CPUTune struct {
		VCPUPins []struct {
			CPUSet string `json:"cpu_set"`
			VCPU   string `json:"vcpu"`
		}
	}
	Level    string `json:"level"`
	Mode     string `json:"mode"`
	Name     string `json:"name"`
	Speed    string `json:"speed"`
	Topology struct {
		Cores   string `json:"cores"`
		Sockets string `json:"json"`
		Threads string `json:"threads"`
	} `json:"topology"`
	Type string `json:"type"`
}

// CPUProfile see: https://engine.domain.tld/ovirt-engine/apidoc/#types/cpu_profile
type CPUProfile struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	Cluster     *Cluster      `json:"cluster"`
	Permissions []*Permission `json:"permissions"`
	QOS         *QOS          `json:"qos"`
}

// CustomProperty see: https://engine.domain.tld/ovirt-engine/apidoc/#types/custom_property
type CustomProperty struct {
	// Attributes
	Name   string `json:"name"`
	Regexp string `json:"regexp"`
	Value  string `json:"value"`
}

// DataCenter see: https://engine.domain.tld/ovirt-engine/apidoc/#types/data_center
type DataCenter struct {
	// Attributes
	Comment           string    `json:"comment"`
	Description       string    `json:"description"`
	ID                string    `json:"id"`
	Local             string    `json:"local"`
	Name              string    `json:"name"`
	QuotaMode         string    `json:"quota_mode"`
	Status            string    `json:"status"`
	StorageFormat     string    `json:"storage_format"`
	SupportedVersions []Version `json:"supported_versions"`
	Version           Version   `json:"version"`
	// Links
	Clusters       []*Cluster       `json:"clusters"`
	ISCSIBonds     []*ISCSIBond     `json:"iscsi_bonds"`
	MACPool        *MACPool         `json:"mac_pool"`
	Networks       []*Network       `json:"networks"`
	Permissions    []*Permission    `json:"permissions"`
	QOSs           []*QOS           `json:"qoss"`
	Quotas         []*Quota         `json:"quotas"`
	StorageDomains []*StorageDomain `json:"storage_domain"`
}

// Device see: https://engine.domain.tld/ovirt-engine/apidoc/#types/device
type Device struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	InstanceType *InstanceType `json:"instance_type"`
	Template     *Template     `json:"template"`
	VM           *VM           `json:"vm"`
	VMs          []*VM         `json:"v_ms"`
}

// Disk see: https://engine.domain.tld/ovirt-engine/apidoc/#types/disk
type Disk struct {
	// Attributes
	Active              string      `json:"active"`
	ActualSize          string      `json:"actual_size"`
	Alias               string      `json:"alias"`
	Bootable            string      `json:"bootable"`
	Comment             string      `json:"comment"`
	Description         string      `json:"description"`
	Format              string      `json:"format"`
	ID                  string      `json:"id"`
	ImageID             string      `json:"image_id"`
	InitialSize         string      `json:"initial_size"`
	Interface           string      `json:"interface"`
	LogicalName         string      `json:"logical_name"`
	LUNStorage          HostStorage `json:"lun_storage"`
	Name                string      `json:"name"`
	PropagateErrors     string      `json:"propagate_errors"`
	ProvisionedSize     string      `json:"provisioned_size"`
	QCOWVersion         string      `json:"qcow_version"`
	ReadOnly            string      `json:"read_only"`
	SGIO                string      `json:"sgio"`
	Shareable           string      `json:"shareable"`
	Sparse              string      `json:"sparse"`
	Status              string      `json:"status"`
	StorageType         string      `json:"storage_type"`
	UsesSCSIReservation string      `json:"uses_scsi_reservation"`
	WipeAfterDelete     string      `json:"wipe_after_delete"`
	// Link
	DiskProfile         *DiskProfile         `json:"disk_profile"`
	InstanceType        *InstanceType        `json:"instance_type"`
	OpenstackVolumeType *OpenStackVolumeType `json:"openstack_volume_type"`
	Permissions         []*Permission        `json:"permissions"`
	Quota               *Quota               `json:"quota"`
	Snapshot            *Snapshot            `json:"snapshot"`
	Statistics          []*Statistic         `json:"statistics"`
	StorageDomain       *StorageDomain       `json:"storage_domain"`
	StorageDomains      []*StorageDomain     `json:"storage_domains"`
	Template            *Template            `json:"template"`
	VM                  *VM                  `json:"vm"`
	VMs                 []*VM                `json:"vms"`
}

// DiskAttachment see: https://engine.domain.tld/ovirt-engine/apidoc/#types/disk_attachment
type DiskAttachment struct {
	// Attributes
	Active              string `json:"active"`
	Bootable            string `json:"bootable"`
	Comment             string `json:"comment"`
	Description         string `json:"description"`
	ID                  string `json:"id"`
	Interface           string `json:"interface"`
	LogicalName         string `json:"logical_name"`
	Name                string `json:"name"`
	PassDiscard         string `json:"pass_discard"`
	ReadOnly            string `json:"read_only"`
	UsesSCSIReservation string `json:"uses_scsi_reservation"`
	// Links
	Disk     *Disk     `json:"disk"`
	Template *Template `json:"template"`
	VM       *VM       `json:"vm"`
}

// DiskProfile see: https://engine.domain.tld/ovirt-engine/apidoc/#types/disk_profile
type DiskProfile struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	Permissions   []*Permission  `json:"permissions"`
	QOS           *QOS           `json:"qos"`
	StorageDomain *StorageDomain `json:"storage_domain"`
}

// DiskSnapshot see: https://engine.domain.tld/ovirt-engine/apidoc/#types/disk_snapshot
type DiskSnapshot struct {
	// Attributes
	Active              string      `json:"active"`
	ActualSize          string      `json:"actual_size"`
	Alias               string      `json:"alias"`
	Bootable            string      `json:"bootable"`
	Comment             string      `json:"comment"`
	Description         string      `json:"description"`
	Format              string      `json:"format"`
	ID                  string      `json:"id"`
	ImageID             string      `json:"image_id"`
	InitialSize         string      `json:"initial_size"`
	Interface           string      `json:"interface"`
	LogicalName         string      `json:"logical_name"`
	LUNStorage          HostStorage `json:"lun_storage"`
	Name                string      `json:"name"`
	PropagateErrors     string      `json:"propagate_errors"`
	ProvisionedSize     string      `json:"provisioned_size"`
	QCOWVersion         string      `json:"qcow_version"`
	ReadOnly            string      `json:"read_only"`
	SGIO                string      `json:"sgio"`
	Shareable           string      `json:"shareable"`
	Sparse              string      `json:"sparse"`
	Status              string      `json:"status"`
	StorageType         string      `json:"storage_type"`
	UsesSCSIReservation string      `json:"uses_scsi_reservation"`
	WipeAfterDelete     string      `json:"wipe_after_delete"`
	// Link
	Disk                *Disk                `json:"disk"`
	DiskProfile         *DiskProfile         `json:"disk_profile"`
	InstanceType        *InstanceType        `json:"instance_type"`
	OpenstackVolumeType *OpenStackVolumeType `json:"openstack_volume_type"`
	Permissions         []*Permission        `json:"permissions"`
	Quota               *Quota               `json:"quota"`
	Snapshot            *Snapshot            `json:"snapshot"`
	Statistics          []*Statistic         `json:"statistics"`
	StorageDomain       *StorageDomain       `json:"storage_domain"`
	StorageDomains      []*StorageDomain     `json:"storage_domains"`
	Template            *Template            `json:"template"`
	VM                  *VM                  `json:"vm"`
	VMs                 []*VM                `json:"vms"`
}

// Display see: https://engine.domain.tld/ovirt-engine/apidoc/#types/display
type Display struct {
	// Attributes
	Address             string      `json:"address"`
	AllowOverride       string      `json:"allow_override"`
	Certificate         Certificate `json:"certificate"`
	CopyPasteEnabled    string      `json:"copy_paste_enabled"`
	DisconnectAction    string      `json:"disconnect_action"`
	FileTransferEnabled string      `json:"file_transfer_enabled"`
	KeyboardLayout      string      `json:"keyboard_layout"`
	Monitors            string      `json:"monitors"`
	Port                string      `json:"port"`
	Proxy               string      `json:"proxy"`
	SecurePort          string      `json:"secure_port"`
	SingleQXLPCI        string      `json:"single_qxl_pci"`
	SmartcardEnabled    string      `json:"smartcard_enabled"`
	Type                string      `json:"type"`
}

// DNS see: https://engine.domain.tld/ovirt-engine/apidoc/#types/dns
type DNS struct {
	// Attributes
	SearchDomains []*Host `json:"search_domains"`
	Servers       []*Host `json:"servers"`
}

// DNSResolverConfiguration see: https://engine.domain.tld/ovirt-engine/apidoc/#types/dns_resolver_configuration
type DNSResolverConfiguration struct {
	// Attributes
	NameServers []string `json:"name_servers"`
}

// Domain see: https://engine.domain.tld/ovirt-engine/apidoc/#types/domain
type Domain struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	User        string `json:"user"`
	// Links
	Groups []*Group `json:"groups"`
	Users  []*User  `json:"users"`
}

// ErrorHandling see: https://engine.domain.tld/ovirt-engine/apidoc/#types/error_handling
type ErrorHandling struct {
	// Attributes
	OnError string `json:"on_error"`
}

// ExternalComputeResource see: https://engine.domain.tld/ovirt-engine/apidoc/#types/external_compute_resource
type ExternalComputeResource struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	Provider    string `json:"provider"`
	URL         string `json:"url"`
	User        string `json:"user"`
	// Links
	ExternalHostProvider *ExternalHostProvider `json:"external_host_provider"`
}

// ExternalDiscoveredHost see: https://engine.domain.tld/ovirt-engine/apidoc/#types/external_discovered_host
type ExternalDiscoveredHost struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	IP          string `json:"ip"`
	LastReport  string `json:"last_report"`
	MAC         string `json:"mac"`
	Name        string `json:"name"`
	SubnetName  string `json:"subnet_name"`
	// Links
	ExternalHostProvider *ExternalHostProvider `json:"external_host_provider"`
}

// ExternalHostGroup see: https://engine.domain.tld/ovirt-engine/apidoc/#types/external_host_group
type ExternalHostGroup struct {
	// Attributes
	ArchitectureName    string `json:"architecture_name"`
	Comment             string `json:"comment"`
	Description         string `json:"description"`
	DomainName          string `json:"domain_name"`
	ID                  string `json:"id"`
	Name                string `json:"name"`
	OperatingSystemName string `json:"operating_system_name"`
	SubnetName          string `json:"subnet_name"`
	// Links
	ExternalHostProvider *ExternalHostProvider `json:"external_host_provider"`
}

// ExternalHostProvider see: https://engine.domain.tld/ovirt-engine/apidoc/#types/external_host_provider
type ExternalHostProvider struct {
	// Attributes
	AuthenticationURL      string     `json:"authentication_url"`
	Comment                string     `json:"comment"`
	Description            string     `json:"description"`
	ID                     string     `json:"id"`
	Name                   string     `json:"name"`
	Password               string     `json:"password"`
	Properties             []Property `json:"properties"`
	RequiresAuthentication string     `json:"requires_authentication"`
	URL                    string     `json:"url"`
	Username               string     `json:"username"`
	// Links
	Certificates     []*Certificate             `json:"certificates"`
	ComputeResources []*ExternalComputeResource `json:"compute_resources"`
	DiscoveredHosts  []*ExternalDiscoveredHost  `json:"discovered_hosts"`
	HostGroups       []*ExternalHostGroup       `json:"host_groups"`
	Hosts            []*Host                    `json:"hosts"`
}

// FencingPolicy see: https://engine.domain.tld/ovirt-engine/apidoc/#types/fencing_policy
type FencingPolicy struct {
	// Attributes
	Enabled                   string                   `json:"enabled"`
	SkipIfConnectivityBroken  SkipIfConnectivityBroken `json:"skip_if_connectivity_broken"`
	SkipIfGlusterBricksUp     string                   `json:"skip_if_gluster_bricks_up"`
	SkipIfGlusterQuorumNotMet string                   `json:"skip_if_gluster_quorum_not_met"`
	SkipIfSDActive            SkipIfSDActive           `json:"skip_if_sd_active"`
}

// File see: https://engine.domain.tld/ovirt-engine/apidoc/#types/file
type File struct {
	// Attributes
	Comment     string `json:"comment"`
	Content     string `json:"content"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	Type        string `json:"type"`
	// Links
	StorageDomain *StorageDomain `json:"storage_domain"`
}

// Filter see: https://engine.domain.tld/ovirt-engine/apidoc/#types/filter
type Filter struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	Position    string `json:"position"`
	// Links
	SchedulingPolicyUnit *SchedulingPolicyUnit `json:"scheduling_policy_unit"`
}

// Floppy see: https://engine.domain.tld/ovirt-engine/apidoc/#types/floppy
type Floppy struct {
	// Attributes
	Comment     string `json:"comment"`
	Content     string `json:"content"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	InstanceType *InstanceType `json:"instance_type"`
	Template     *Template     `json:"template"`
	VM           *VM           `json:"vm"`
	VMs          []*VM         `json:"vms"`
}

// GlusterBrick see: https://engine.domain.tld/ovirt-engine/apidoc/#types/gluster_brick
type GlusterBrick struct {
	// Attributes
	BrickDir       string              `json:"brick_dir"`
	Comment        string              `json:"comment"`
	Description    string              `json:"description"`
	Device         string              `json:"device"`
	FSName         string              `json:"fs_name"`
	GlusterClients []GlusterClient     `json:"gluster_clients"`
	ID             string              `json:"id"`
	MemoryPool     []GlusterMemoryPool `json:"memory_pool"`
	MNTOptions     string              `json:"mnt_options"`
	Name           string              `json:"name"`
	PID            string              `json:"pid"`
	Port           string              `json:"port"`
	ServerID       string              `json:"server_id"`
	Status         string              `json:"status"`
	// Links
	GlusterVolume *GlusterVolume `json:"gluster_volume"`
	InstanceType  *InstanceType  `json:"instance_type"`
	Statistics    []*Statistic   `json:"statistics"`
	Template      *Template      `json:"template"`
	VM            *VM            `json:"vm"`
	VMs           []*VM          `json:"vms"`
}

// GlusterClient see: https://engine.domain.tld/ovirt-engine/apidoc/#types/gluster_client
type GlusterClient struct {
	// Attributes
	BytesRead    string `json:"bytes_read"`
	BytesWritten string `json:"bytes_written"`
	ClientPort   string `json:"client_port"`
	HostName     string `json:"host_name"`
}

// GlusterHook see: https://engine.domain.tld/ovirt-engine/apidoc/#types/gluster_hook
type GlusterHook struct {
	// Attributes
	Checksum       string `json:"checksum"`
	Comment        string `json:"comment"`
	ConflictStatus string `json:"conflict_status"`
	Conflicts      string `json:"conflicts"`
	Content        string `json:"content"`
	ContentType    string `json:"content_type"`
	Description    string `json:"description"`
	GlusterCommand string `json:"gluster_command"`
	ID             string `json:"id"`
	Name           string `json:"name"`
	Stage          string `json:"stage"`
	Status         string `json:"status"`
	// Links
	Cluster     *Cluster             `json:"cluster"`
	ServerHooks []*GlusterServerHook `json:"server_hooks"`
}

// GlusterMemoryPool see: https://engine.domain.tld/ovirt-engine/apidoc/#types/gluster_memory_pool
type GlusterMemoryPool struct {
	// Attributes
	AllocCount  string `json:"alloc_count"`
	ColdCount   string `json:"cold_count"`
	Comment     string `json:"comment"`
	Description string `json:"description"`
	HOTCount    string `json:"hot_count"`
	ID          string `json:"id"`
	MAXAlloc    string `json:"max_alloc"`
	MAXStdAlloc string `json:"max_std_alloc"`
	Name        string `json:"name"`
	PaddedSize  string `json:"padded_size"`
	PoolMisses  string `json:"pool_misses"`
	Type        string `json:"type"`
}

// GlusterServerHook see: https://engine.domain.tld/ovirt-engine/apidoc/#types/gluster_server_hook
type GlusterServerHook struct {
	// Attributes
	Checksum    string `json:"checksum"`
	Comment     string `json:"comment"`
	ContentType string `json:"content_type"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	Status      string `json:"status"`
	// Links
	Host Host `json:"host"`
}

// GlusterVolume see: https://engine.domain.tld/ovirt-engine/apidoc/#types/gluster_volume
type GlusterVolume struct {
	// Attributes
	Comment         string   `json:"comment"`
	Description     string   `json:"description"`
	DisperseCount   string   `json:"disperse_count"`
	ID              string   `json:"id"`
	Name            string   `json:"name"`
	Options         []Option `json:"options"`
	RedundancyCount string   `json:"redundancy_count"`
	ReplicaCount    string   `json:"replica_count"`
	Status          string   `json:"status"`
	StripeCount     string   `json:"stripe_count"`
	TransportTypes  []string `json:"transport_types"`
	VolumeType      string   `json:"volume_type"`
	// Links
	Bricks     []*GlusterBrick `json:"bricks"`
	Cluster    *Cluster        `json:"cluster"`
	Statistics []*Statistic    `json:"statistics"`
}

// GraphicsConsole see: https://engine.domain.tld/ovirt-engine/apidoc/#types/graphics_console
type GraphicsConsole struct {
	// Attributes
	Address     string `json:"address"`
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	Port        string `json:"port"`
	Protocol    string `json:"protocol"`
	TLSPort     string `json:"tls_port"`
	// Link
	InstanceType *InstanceType `json:"instance_type"`
	Template     *Template     `json:"template"`
	VM           *VM           `json:"vm"`
}

// Group see: https://engine.domain.tld/ovirt-engine/apidoc/#types/group
type Group struct {
	// Attributes
	Comment       string `json:"comment"`
	Description   string `json:"description"`
	DomainEntryID string `json:"domain_entry_id"`
	ID            string `json:"id"`
	Name          string `json:"name"`
	Namespace     string `json:"namespace"`
	// Link
	Domain      *Domain       `json:"domain"`
	Permissions []*Permission `json:"permissions"`
	Roles       []*Role       `json:"roles"`
	Tags        []*Tag        `json:"tags"`
}

// GuestOperatingSystem see: https://engine.domain.tld/ovirt-engine/apidoc/#types/guest_operating_system
type GuestOperatingSystem struct {
	// Attributes
	Architecture string `json:"architecture"`
	Codename     string `json:"codename"`
	Distribution string `json:"distribution"`
	Family       string `json:"family"`
	Kernel       struct {
		Version Version `json:"version"`
	} `json:"kernel"`
	Version Version `json:"version"`
}

// HardwareInformation see: https://engine.domain.tld/ovirt-engine/apidoc/#types/hardware_information
type HardwareInformation struct {
	// Attributes
	Family              string `json:"family"`
	Manufacturer        string `json:"manufacturer"`
	ProductName         string `json:"product_name"`
	SerialNumber        string `json:"serial_number"`
	SupportedRNGSources string `json:"supported_rng_sources"`
	UUID                string `json:"uuid"`
	Version             string `json:"version"`
}

// HighAvailability see: https://engine.domain.tld/ovirt-engine/apidoc/#types/high_availability
type HighAvailability struct {
	// Attributes
	Enabled  string `json:"enabled"`
	Priority string `json:"priority"`
}

// Hook see: https://engine.domain.tld/ovirt-engine/apidoc/#types/hook
type Hook struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	EventName   string `json:"event_name"`
	ID          string `json:"id"`
	MD5         string `json:"md5"`
	Name        string `json:"name"`
	// Links
	Host *Host `json:"host"`
}

// Host see: https://engine.domain.tld/ovirt-engine/apidoc/#types/host
type Host struct {
	// Attributes
	Address           string      `json:"address"`
	AutoNumaStatus    string      `json:"auto_numa_status"`
	Certificate       Certificate `json:"certificate"`
	Comment           string      `json:"comment"`
	CPU               CPU         `json:"cpu"`
	Description       string      `json:"description"`
	DevicePassthrough struct {
		Enabled string `json:"enabled"`
	} `json:"device_passthrough"`
	Display             Display             `json:"display"`
	ExternalStatus      string              `json:"external_status"`
	HardwareInformation HardwareInformation `json:"hardware_information"`
	HostedEngine        HostedEngine        `json:"hosted_engine"`
	ID                  string              `json:"id"`
	ISCSI               ISCSIDetails        `json:"iscsi"`
	KdumpStatus         string              `json:"kdump_status"`
	KSM                 struct {
		Enabled          string `json:"enabled"`
		MergeAcrossNodes string `json:"merge_across_nodes"`
	} `json:"ksm"`
	LibvirtVersion      Version         `json:"libvirt_version"`
	MaxSchedulingMemory string          `json:"max_scheduling_memory"`
	Memory              string          `json:"memory"`
	Name                string          `json:"name"`
	NumaSupported       string          `json:"numa_supported"`
	OS                  OperatingSystem `json:"os"`
	OverrideIptables    string          `json:"override_iptables"`
	Port                string          `json:"port"`
	PowerManagement     PowerManagement `json:"power_management"`
	Protocol            string          `json:"protocol"`
	RootPassword        string          `json:"root_password"`
	SeLinux             struct {
		Mode string `json:"mode"`
	} `json:"se_linux"`
	SPM struct {
		Priority string `json:"priority"`
		Status   string `json:"status"`
	} `json:"spm"`
	SSH          SSH    `json:"ssh"`
	Status       string `json:"status"`
	StatusDetail string `json:"status_detail"`
	Summary      struct {
		Active    string `json:"active"`
		Migrating string `json:"migrating"`
		Total     string `json:"total"`
	} `json:"summary"`
	TransparentHugePages struct {
		Enabled string `json:"enabled"`
	} `json:"transparent_huge_pages"`
	Type            string  `json:"type"`
	UpdateAvailable string  `json:"update_available"`
	Version         Version `json:"version"`
	// Links
	AffinityLabels              []*AffinityLabel              `json:"affinity_labels"`
	Agents                      []*Agent                      `json:"agents"`
	Cluster                     *Cluster                      `json:"cluster"`
	Devices                     []*Device                     `json:"devices"`
	ExternalHostProvider        *ExternalHostProvider         `json:"external_host_provider"`
	Hooks                       []*Hook                       `json:"hooks"`
	KatelloErrata               *KatelloErratum               `json:"katello_errata"`
	NetworkAttachments          []*NetworkAttachment          `json:"network_attachments"`
	NICs                        []*HostNIC                    `json:"nics"`
	NumaNodes                   []*NumaNode                   `json:"numa_nodes"`
	Permissions                 []*Permission                 `json:"permissions"`
	Statistics                  []*Statistic                  `json:"statistics"`
	StorageConnectionExtensions []*StorageConnectionExtension `json:"storage_connection_extensions"`
	Storages                    []*HostStorage                `json:"storages"`
	Tags                        []*Tag                        `json:"tags"`
	UnmanagedNetworks           []*UnmanagedNetwork           `json:"unmanaged_networks"`
}

// HostDevice see: https://engine.domain.tld/ovirt-engine/apidoc/#types/host_device
type HostDevice struct {
	// Attributes
	Capability       string      `json:"capability"`
	Comment          string      `json:"comment"`
	Description      string      `json:"description"`
	Driver           string      `json:"driver"`
	ID               string      `json:"id"`
	IOMMUGroup       string      `json:"iommu_group"`
	Name             string      `json:"name"`
	PhysicalFunction *HostDevice `json:"physical_function"`
	Placeholder      string      `json:"placeholder"`
	Product          Product     `json:"product"`
	Vendor           Vendor      `json:"vendor"`
	VirtualFunctions string      `json:"virtual_functions"`
	// Links
	Host         *Host       `json:"host"`
	ParentDevice *HostDevice `json:"parent_device"`
	VM           *VM         `json:"vm"`
}

// HostNIC see: https://engine.domain.tld/ovirt-engine/apidoc/#types/host_nic
type HostNIC struct {
	// Attributes
	ADAggregatorID                string                               `json:"ad_aggregator_id"`
	BaseInterface                 string                               `json:"base_interface"`
	Bonding                       Bonding                              `json:"bonding"`
	BootProtocol                  string                               `json:"boot_protocol"`
	Bridged                       string                               `json:"bridged"`
	CheckConnectivity             string                               `json:"check_connectivity"`
	Comment                       string                               `json:"comment"`
	CustomConfiguration           string                               `json:"custom_configuration"`
	Description                   string                               `json:"description"`
	ID                            string                               `json:"id"`
	IP                            IP                                   `json:"ip"`
	IPv6                          IP                                   `json:"i_pv6"`
	IPv6BootProtocol              string                               `json:"i_pv6_boot_protocol"`
	MAC                           MAC                                  `json:"mac"`
	MTU                           string                               `json:"mtu"`
	Name                          string                               `json:"name"`
	NetworkLabels                 []NetworkLabel                       `json:"network_labels"`
	OverrideConfiguration         string                               `json:"override_configuration"`
	Properties                    []Property                           `json:"properties"`
	Speed                         string                               `json:"speed"`
	Statistics                    []Statistic                          `json:"statistics"`
	Status                        string                               `json:"status"`
	VirtualFunctionsConfiguration HostNICVirtualFunctionsConfiguration `json:"virtual_functions_configuration"`
	VLAN                          VLAN                                 `json:"vlan"`
}

// HostNICVirtualFunctionsConfiguration see: https://engine.domain.tld/ovirt-engine/apidoc/#types/host_nic_virtual_functions_configuration
type HostNICVirtualFunctionsConfiguration struct {
	// Attributes
	AllNetworksAllowed          string `json:"all_networks_allowed"`
	MaxNumberOfVirtualFunctions string `json:"max_number_of_virtual_functions"`
	NumberOfVirtualFunctions    string `json:"number_of_virtual_functions"`
}

// HostStorage see: https://engine.domain.tld/ovirt-engine/apidoc/#types/host_storage
type HostStorage struct {
	// Attributes
	Address      string        `json:"address"`
	Comment      string        `json:"comment"`
	Description  string        `json:"description"`
	ID           string        `json:"id"`
	LogicalUnits []LogicalUnit `json:"logical_units"`
	MountOptions string        `json:"mount_options"`
	Name         string        `json:"name"`
	NFSRetrans   string        `json:"nfs_retrans"`
	NFSTimeo     string        `json:"nfs_timeo"`
	NFSVersion   string        `json:"nfs_version"`
	OverrideLUNS string        `json:"override_luns"`
	Password     string        `json:"password"`
	Path         string        `json:"path"`
	Port         string        `json:"port"`
	Portal       string        `json:"portal"`
	Target       string        `json:"target"`
	Type         string        `json:"type"`
	Username     string        `json:"username"`
	VFSType      string        `json:"vfs_type"`
	VolumeGroup  VolumeGroup   `json:"volume_group"`
	// Links
	Host *Host `json:"host"`
}

// HostedEngine see: https://engine.domain.tld/ovirt-engine/apidoc/#types/host_engine
type HostedEngine struct {
	// Attributes
	Active            string `json:"active"`
	Configured        string `json:"configured"`
	GlobalMaintenance string `json:"global_maintenance"`
	LocalMaintenance  string `json:"local_maintenance"`
	Score             string `json:"score"`
}

// Icon see: https://engine.domain.tld/ovirt-engine/apidoc/#types/icon
type Icon struct {
	// Attributes
	Comment     string `json:"comment"`
	Data        string `json:"data"`
	Description string `json:"description"`
	ID          string `json:"id"`
	MediaType   string `json:"media_type"`
	Name        string `json:"name"`
}

// Image see: https://engine.domain.tld/ovirt-engine/apidoc/#types/image
type Image struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	StorageDomain *StorageDomain `json:"storage_domain"`
}

// Initialization see: https://engine.domain.tld/ovirt-engine/apidoc/#types/initialization
type Initialization struct {
	// Attributes
	ActiveDirectoryOU string `json:"active_directory_ou"`
	AuthorizedSSHKeys string `json:"authorized_ssh_keys"`
	CloudInit         struct {
		AuthorizedKeys []struct {
			Comment     string `json:"comment"`
			Description string `json:"description"`
			ID          string `json:"id"`
			Key         string `json:"key"`
			Name        string `json:"name"`
		} `json:"authorized_keys"`
		Files                []File               `json:"files"`
		Host                 Host                 `json:"host"`
		NetworkConfiguration NetworkConfiguration `json:"network_configuration"`
		RegenerateSSHKeys    string               `json:"regenerate_ssh_keys"`
		Timezone             string               `json:"timezone"`
		Users                []User               `json:"users"`
	} `json:"cloud_init"`
	Configuration struct {
		Data string `json:"data"`
		Type string `json:"type"`
	} `json:"configuration"`
	CustomScript      string             `json:"custom_script"`
	DNSSearch         string             `json:"dns_search"`
	DNSServers        string             `json:"dns_servers"`
	Domain            string             `json:"domain"`
	HostName          string             `json:"host_name"`
	InputLocale       string             `json:"input_locale"`
	NICConfigurations []NICConfiguration `json:"nic_configurations"`
	OrgName           string             `json:"org_name"`
	RegenerateIDs     string             `json:"regenerate_i_ds"`
	RegenerateSSHKeys string             `json:"regenerate_ssh_keys"`
	RootPassword      string             `json:"root_password"`
	SystemLocale      string             `json:"system_locale"`
	Timezone          string             `json:"timezone"`
	UILanguage        string             `json:"ui_language"`
	UserLocale        string             `json:"user_locale"`
	UserName          string             `json:"user_name"`
	WindowsLicenseKey string             `json:"windows_license_key"`
}

// InstanceType see: https://engine.domain.tld/ovirt-engine/apidoc/#types/instance_type
type InstanceType struct {
	// Attributes
	BIOS                       BIOS               `json:"bios"`
	Comment                    string             `json:"comment"`
	Console                    Console            `json:"console"`
	CPU                        CPU                `json:"cpu"`
	CPUShares                  string             `json:"cpu_shares"`
	CreationTime               int                `json:"creation_time"`
	CustomCompatabilityVersion Version            `json:"custom_compatability_version"`
	CustomCPUModel             string             `json:"custom_cpu_model"`
	CustomEmulatedMachine      string             `json:"custom_emulated_machine"`
	CustomProperties           []CustomProperty   `json:"custom_properties"`
	DeleteProtected            string             `json:"delete_protected"`
	Description                string             `json:"description"`
	Display                    Display            `json:"display"`
	Domain                     Domain             `json:"domain"`
	HighAvailability           HighAvailability   `json:"high_availability"`
	ID                         string             `json:"id"`
	Initialization             Initialization     `json:"initialization"`
	IO                         IO                 `json:"io"`
	LargeIcon                  Icon               `json:"large_icon"`
	Lease                      StorageDomainLease `json:"lease"`
	Memory                     int                `json:"memory"`
	MemoryPolicy               MemoryPolicy       `json:"memory_policy"`
	Migration                  MigrationOptions   `json:"migration"`
	MigrationDowntime          string             `json:"migration_downtime"`
	Name                       string             `json:"name"`
	Origin                     string             `json:"origin"`
	OS                         OperatingSystem    `json:"os"`
	RNGDevice                  RNGDevice          `json:"rng_device"`
	SerialNumber               SerialNumber       `json:"serial_number"`
	SmallIcon                  Icon               `json:"small_icon"`
	SoundcardEnabled           string             `json:"soundcard_enabled"`
	SSO                        SSO                `json:"sso"`
	StartPaused                string             `json:"start_paused"`
	Stateless                  string             `json:"stateless"`
	Status                     string             `json:"status"`
	TimeZone                   TimeZone           `json:"time_zone"`
	TunnelMigration            string             `json:"tunnel_migration"`
	Type                       string             `json:"type"`
	USB                        USB                `json:"usb"`
	Version                    TemplateVersion    `json:"version"`
	VirtioSCSI                 VirtioSCSI         `json:"virtio_scsi"`
	VM                         VM                 `json:"vm"`
	// Links
	CDROMs           []*CDROM           `json:"cdroms"`
	Cluster          *Cluster           `json:"cluster"`
	CPUProfile       *CPUProfile        `json:"cpu_profile"`
	DiskAttachments  []*DiskAttachment  `json:"disk_attachments"`
	GraphicsConsoles []*GraphicsConsole `json:"graphics_consoles"`
	NICs             []*NIC             `json:"nics"`
	Permissions      []*Permission      `json:"permissions"`
	Quota            *Quota             `json:"quota"`
	StorageDomain    *StorageDomain     `json:"storage_domain"`
	Tags             []*Tag             `json:"tags"`
	Watchdogs        []*Watchdog        `json:"watchdogs"`
}

// IO see: https://engine.domain.tld/ovirt-engine/apidoc/#types/io
type IO struct {
	// Attributes
	Threads string `json:"threads"`
}

// IP see: https://engine.domain.tld/ovirt-engine/apidoc/#types/ip
type IP struct {
	// Attributes
	Address string `json:"address"`
	Gateway string `json:"gateway"`
	Netmask string `json:"netmask"`
	Version string `json:"version"`
}

// IPAddressAssignment see: https://engine.domain.tld/ovirt-engine/apidoc/#types/ip_address_assignment
type IPAddressAssignment struct {
	// Attributes
	AssignmentMethod string `json:"assignment_method"`
	IP               IP     `json:"ip"`
}

// ISCSIBond see: https://engine.domain.tld/ovirt-engine/apidoc/#types/iscsi_bond
type ISCSIBond struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	DataCenter         *DataCenter          `json:"data_center"`
	Networks           []*Network           `json:"networks"`
	StorageConnections []*StorageConnection `json:"storage_connections"`
}

// ISCSIDetails see: https://engine.domain.tld/ovirt-engine/apidoc/#types/iscsi_details
type ISCSIDetails struct {
	// Attributes
	Address         string `json:"address"`
	DiskID          string `json:"disk_id"`
	Initiator       string `json:"initiator"`
	LUNMapping      string `json:"lun_mapping"`
	Password        string `json:"password"`
	Paths           string `json:"paths"`
	Port            string `json:"port"`
	Portal          string `json:"portal"`
	ProductID       string `json:"product_id"`
	Serial          string `json:"serial"`
	Size            string `json:"size"`
	Status          string `json:"status"`
	StorageDomainID string `json:"storage_domain_id"`
	Targe           string `json:"targe"`
	Username        string `json:"username"`
	VendorID        string `json:"vendor_id"`
	VolumeGroupID   string `json:"volume_group_id"`
}

// Job see: https://engine.domain.tld/ovirt-engine/apidoc/#types/job
type Job struct {
	// Attributes
	AutoCleared string `json:"auto_cleared"`
	Comment     string `json:"comment"`
	Description string `json:"description"`
	EndTime     int    `json:"end_time"`
	External    string `json:"external"`
	ID          string `json:"id"`
	LastUpdated int    `json:"last_updated"`
	Name        string `json:"name"`
	StartTime   int    `json:"start_time"`
	Status      string `json:"status"`
	// Links
	Owner *User   `json:"owner"`
	Steps []*Step `json:"steps"`
}

// KatelloErratum see: https://engine.domain.tld/ovirt-engine/apidoc/#types/katello_erratum
type KatelloErratum struct {
	// Attributes
	Comment     string    `json:"comment"`
	Description string    `json:"description"`
	ID          string    `json:"id"`
	Issued      int       `json:"issued"`
	Name        string    `json:"name"`
	Packages    []Package `json:"packages"`
	Serverity   string    `json:"serverity"`
	Solution    string    `json:"solution"`
	Summary     string    `json:"summary"`
	Title       string    `json:"title"`
	Type        string    `json:"type"`
	// Links
	Host *Host `json:"host"`
	VM   *VM   `json:"vm"`
}

// KSM see: https://engine.domain.tld/ovirt-engine/apidoc/#types/ksm
type KSM struct {
	// Attributes
	Enabled          string `json:"enabled"`
	MergeAcrossNodes string `json:"merge_across_nodes"`
}

// Link see: https://engine.domain.tld/ovirt-engine/apidoc/#types/link
type Link struct {
	// Attributes
	Href string `json:"href"`
	Rel  string `json:"rel"`
}

// LogicalUnit see: https://engine.domain.tld/ovirt-engine/apidoc/#types/logical_unit
type LogicalUnit struct {
	// Attributes
	Address           string `json:"address"`
	DiscardMaxSize    string `json:"discard_max_size"`
	DiscardZeroesData string `json:"discard_zeroes_data"`
	DiskID            string `json:"disk_id"`
	ID                string `json:"id"`
	LUNMapping        string `json:"lun_mapping"`
	Password          string `json:"password"`
	Paths             string `json:"paths"`
	Port              string `json:"port"`
	Portal            string `json:"portal"`
	ProductID         string `json:"product_id"`
	Serial            string `json:"serial"`
	Size              string `json:"size"`
	Status            string `json:"status"`
	StorageDomainID   string `json:"storage_domain_id"`
	Target            string `json:"target"`
	Username          string `json:"username"`
	VendorID          string `json:"vendor_id"`
	VolumeGroupID     string `json:"volume_group_id"`
}

// MAC see: https://engine.domain.tld/ovirt-engine/apidoc/#types/mac
type MAC struct {
	// Attributes
	Address string `json:"address"`
}

// MACPool see: https://engine.domain.tld/ovirt-engine/apidoc/#types/mac_pool
type MACPool struct {
	// Attributes
	AllowDuplicates string  `json:"allow_duplicates"`
	Comment         string  `json:"comment"`
	DefaultPool     string  `json:"default_pool"`
	Description     string  `json:"description"`
	ID              string  `json:"id"`
	Name            string  `json:"name"`
	Ranges          []Range `json:"ranges"`
}

// MemoryPolicy see: https://engine.domain.tld/ovirt-engine/apidoc/#types/memory_policy
type MemoryPolicy struct {
	// Attributes
	Ballooning string `json:"ballooning"`
	Guaranteed int    `json:"guaranteed"`
	Max        int    `json:"max"`
	OverCommit struct {
		Percent string `json:"percent"`
	} `json:"over_commit"`
	TransparentHugePages struct {
		Enabled string `json:"enabled"`
	} `json:"transparent_huge_pages"`
}

// MigrationOptions see: https://engine.domain.tld/ovirt-engine/apidoc/#types/migration_options
type MigrationOptions struct {
	// Attributes
	AutoConverge string `json:"auto_converge"`
	Bandwidth    struct {
		AssignmentMethod string `json:"assignment_method"`
		CustomValue      string `json:"custom_value"`
	} `json:"bandwidth"`
	Compressed string `json:"compressed"`
	// Links
	Policy *MigrationPolicy `json:"policy"`
}

// MigrationPolicy see: https://engine.domain.tld/ovirt-engine/apidoc/#types/migration_policy
type MigrationPolicy struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
}

// Network see: https://engine.domain.tld/ovirt-engine/apidoc/#types/network
type Network struct {
	// Attributes
	Comment                  string                   `json:"comment"`
	Description              string                   `json:"description"`
	Display                  string                   `json:"display"`
	DNSResolverConfiguration DNSResolverConfiguration `json:"dns_resolver_configuration"`
	ID                       string                   `json:"id"`
	IP                       IP                       `json:"ip"`
	MTU                      string                   `json:"mtu"`
	Name                     string                   `json:"name"`
	ProfileRequired          string                   `json:"profile_required"`
	Required                 string                   `json:"required"`
	Status                   string                   `json:"status"`
	STP                      string                   `json:"stp"`
	Usages                   []string                 `json:"usages"`
	VLAN                     VLAN                     `json:"vlan"`
	// Link
	Cluster       *Cluster        `json:"cluster"`
	DataCenter    *DataCenter     `json:"data_center"`
	NetworkLabels []*NetworkLabel `json:"network_labels"`
	Permissions   []*Permission   `json:"permissions"`
	QOS           *QOS            `json:"qos"`
	VNICProfile   []*VNICProfile  `json:"vnic_profile"`
}

// NetworkAttachment see: https://engine.domain.tld/ovirt-engine/apidoc/#types/network_attachment
type NetworkAttachment struct {
	// Attributes
	Comment                  string                   `json:"comment"`
	Description              string                   `json:"description"`
	DNSResolverConfiguration DNSResolverConfiguration `json:"dns_resolver_configuration"`
	ID                       string                   `json:"id"`
	InSync                   string                   `json:"in_sync"`
	IPAddressAssignments     []IPAddressAssignment    `json:"ip_address_assignments"`
	Name                     string                   `json:"name"`
	Properties               []Property               `json:"properties"`
	ReportedConfigurations   []ReportedConfiguration  `json:"reported_configurations"`
	// Links
	Host    *Host    `json:"host"`
	HostNIC *HostNIC `json:"host_nic"`
	Network *Network `json:"network"`
	QOS     *QOS     `json:"qos"`
}

// NetworkConfiguration see: https://engine.domain.tld/ovirt-engine/apidoc/#types/network_configuration
type NetworkConfiguration struct {
	// Attributes
	DNS  DNS   `json:"dns"`
	NICs []NIC `json:"nics"`
}

// NetworkFilter see: https://engine.domain.tld/ovirt-engine/apidoc/#types/network_filter
type NetworkFilter struct {
	// Attributes
	Comment     string  `json:"comment"`
	Description string  `json:"description"`
	ID          string  `json:"id"`
	Name        string  `json:"name"`
	Version     Version `json:"version"`
}

// NetworkLabel see: https://engine.domain.tld/ovirt-engine/apidoc/#types/network_label
type NetworkLabel struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	HostNIC *HostNIC `json:"host_nic"`
	Network *Network `json:"network"`
}

// NIC see: https://engine.domain.tld/ovirt-engine/apidoc/#types/nic
type NIC struct {
	// Attributes
	BootProtocol string `json:"boot_protocol"`
	Comment      string `json:"comment"`
	Description  string `json:"description"`
	ID           string `json:"id"`
	Interface    string `json:"interface"`
	Linked       string `json:"linked"`
	MAC          MAC    `json:"mac"`
	Name         string `json:"name"`
	OnBoot       string `json:"on_boot"`
	Plugged      string `json:"plugged"`
	// Links
	InstanceType       *InstanceType        `json:"instance_type"`
	Network            *Network             `json:"network"`
	NetworkAttachments []*NetworkAttachment `json:"network_attachments"`
	NetworkLabels      []*NetworkLabel      `json:"network_labels"`
	ReportedDevices    *struct {
		ReportedDevice []ReportedDevice `json:"reported_device"`
	} `json:"reported_devices"`
	Statistics                     []*Statistic    `json:"statistics"`
	Template                       *Template       `json:"template"`
	VirtualFunctionAllowedLabels   []*NetworkLabel `json:"virtual_function_allowed_labels"`
	VirtualFunctionAllowedNetworks []*Network      `json:"virtual_function_allowed_networks"`
	VM                             *VM             `json:"vm"`
	VMs                            []*VM           `json:"v_ms"`
	VNICProfile                    *VNICProfile    `json:"vnic_profile"`
}

// NICConfiguration see: https://engine.domain.tld/ovirt-engine/apidoc/#types/nic_configuration
type NICConfiguration struct {
	// Attributes
	BootProtocol string `json:"boot_protocol"`
	IP           IP     `json:"ip"`
	Name         string `json:"name"`
	OnBoot       string `json:"on_boot"`
}

// NumaNode see: https://engine.domain.tld/ovirt-engine/apidoc/#types/numa_node
type NumaNode struct {
	// Attributes
	Comment      string `json:"comment"`
	CPU          CPU    `json:"cpu"`
	Description  string `json:"description"`
	ID           string `json:"id"`
	Index        string `json:"index"`
	Memory       string `json:"memory"`
	Name         string `json:"name"`
	NodeDistance string `json:"node_distance"`
	// Links
	Host       *Host        `json:"host"`
	Statistics []*Statistic `json:"statistics"`
}

// OpenstackVolumeAuthenticationKey see: https://engine.domain.tld/ovirt-engine/apidoc/#types/openstack_volume_authentication_key
type OpenstackVolumeAuthenticationKey struct {
	// Attributes
	Comment      string `json:"comment"`
	CreationDate int    `json:"creation_date"`
	Description  string `json:"description"`
	ID           string `json:"id"`
	Name         string `json:"name"`
	UsageType    string `json:"usage_type"`
	UUID         string `json:"uuid"`
	Value        string `json:"value"`
	// Links
	OpenstackVolumeProvider *OpenStackVolumeProvider `json:"openstack_volume_provider"`
}

// OpenStackVolumeProvider see: https://engine.domain.tld/ovirt-engine/apidoc/#types/open_stack_volume_provider
type OpenStackVolumeProvider struct {
	// Attributes
	AuthenticationURL      string     `json:"authentication_url"`
	Comment                string     `json:"comment"`
	Description            string     `json:"description"`
	ID                     string     `json:"id"`
	Name                   string     `json:"name"`
	Password               string     `json:"password"`
	Properties             []Property `json:"properties"`
	RequiresAuthentication string     `json:"requires_authentication"`
	TenantName             string     `json:"tenant_name"`
	URL                    string     `json:"url"`
	Username               string     `json:"username"`
	// Links
	AuthenticationKeys *OpenstackVolumeAuthenticationKey `json:"authentication_keys"`
	Certificates       []*Certificate                    `json:"certificates"`
	DataCenter         *DataCenter                       `json:"data_center"`
	VolumeTypes        []*OpenStackVolumeType            `json:"volume_types"`
}

// OpenStackVolumeType see: https://engine.domain.tld/ovirt-engine/apidoc/#types/open_stack_volume_type
type OpenStackVolumeType struct {
	// Attributes
	Comment     string     `json:"comment"`
	Description string     `json:"description"`
	ID          string     `json:"id"`
	Name        string     `json:"name"`
	Properties  []Property `json:"properties"`
	// Links
	OpenstackVolumeProvider *OpenStackVolumeProvider `json:"openstack_volume_provider"`
}

// OperatingSystem see: https://engine.domain.tld/ovirt-engine/apidoc/#types/operating_system
type OperatingSystem struct {
	// Attributes
	Boot struct {
		Devices struct {
			Device []string `json:"device"`
		} `json:"devices"`
	} `json:"boot"`
	Cmdline               string  `json:"cmdline"`
	CustomKernelCmdline   string  `json:"custom_kernel_cmdline"`
	Initrd                string  `json:"initrd"`
	Kernel                string  `json:"kernel"`
	ReportedKernelCmdline string  `json:"reported_kernel_cmdline"`
	Type                  string  `json:"type"`
	Version               Version `json:"version"`
}

// Option see: https://engine.domain.tld/ovirt-engine/apidoc/#types/option
type Option struct {
	// Attributes
	Name  string `json:"name"`
	Type  string `json:"type"`
	Value string `json:"value"`
}

// Package see: https://engine.domain.tld/ovirt-engine/apidoc/#types/package
type Package struct {
	// Attributes
	Name string `json:"name"`
}

// Payload see: https://engine.domain.tld/ovirt-engine/apidoc/#types/payload
type Payload struct {
	// Attributes
	Files    []File `json:"files"`
	Type     string `json:"type"`
	VolumeID string `json:"volume_id"`
}

// Permission see: https://engine.domain.tld/ovirt-engine/apidoc/#types/permission
type Permission struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	Cluster       *Cluster       `json:"cluster"`
	DataCenter    *DataCenter    `json:"data_center"`
	Disk          *Disk          `json:"disk"`
	Group         *Group         `json:"group"`
	Host          *Host          `json:"host"`
	Role          *Role          `json:"role"`
	StorageDomain *StorageDomain `json:"storage_domain"`
	Template      *Template      `json:"template"`
	User          *User          `json:"user"`
	VM            *VM            `json:"vm"`
	VMPool        *VMPool        `json:"vm_pool"`
}

// PowerManagement see: https://engine.domain.tld/ovirt-engine/apidoc/#types/power_management
type PowerManagement struct {
	// Attributes
	Address            string   `json:"address"`
	Agents             []Agent  `json:"agents"`
	AutomaticPMEnabled string   `json:"automatic_pm_enabled"`
	Enabled            string   `json:"enabled"`
	KdumpDetection     string   `json:"kdump_detection"`
	Option             []Option `json:"option"`
	Password           string   `json:"password"`
	PMProxies          []struct {
		Type string `json:"type"`
	} `json:"pm_proxies"`
	Status   string `json:"status"`
	Type     string `json:"type"`
	Username string `json:"username"`
}

// Product see: https://engine.domain.tld/ovirt-engine/apidoc/#types/product
type Product struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
}

// ProductInfo see: https://engine.domain.tld/ovirt-engine/apidoc/#types/product_info
type ProductInfo struct {
	// Attributes
	Name    string  `json:"name"`
	Vendor  string  `json:"vendor"`
	Version Version `json:"version"`
}

// Property see: https://engine.domain.tld/ovirt-engine/apidoc/#types/property
type Property struct {
	// Attributes
	Name  string `json:"name"`
	Value string `json:"value"`
}

// QOS see: https://engine.domain.tld/ovirt-engine/apidoc/#types/qos
type QOS struct {
	// Attributes
	Comment                   string `json:"comment"`
	CPULimit                  string `json:"cpu_limit"`
	Description               string `json:"description"`
	ID                        string `json:"id"`
	InboundAverage            string `json:"inbound_average"`
	InboundBurst              string `json:"inbound_burst"`
	InboundPeak               string `json:"inbound_peak"`
	MaxIOPS                   string `json:"max_iops"`
	MaxReadIOPS               string `json:"max_read_iops"`
	MaxReadThroughput         string `json:"max_read_throughput"`
	MaxWriteIOPS              string `json:"max_write_iops"`
	MaxWriteThroughput        string `json:"max_write_throughput"`
	Name                      string `json:"name"`
	OutboundAverage           string `json:"outbound_average"`
	OutboundAverageLinkshare  string `json:"outbound_average_linkshare"`
	OutboundAverageRealtime   string `json:"outbound_average_realtime"`
	OutboundAverageUpperlimit string `json:"outbound_average_upperlimit"`
	OutboundBurst             string `json:"outbound_burst"`
	OutboundPeak              string `json:"outbound_peak"`
	Type                      string `json:"type"`
	// Link
	DataCenter *DataCenter `json:"data_center"`
}

// Quota see: https://engine.domain.tld/ovirt-engine/apidoc/#types/quota
type Quota struct {
	// Attributes
	ClusterHardLimitPCT string     `json:"cluster_hard_limit_pct"`
	ClusterSoftLimitPCT string     `json:"cluster_soft_limit_pct"`
	Comment             string     `json:"comment"`
	DataCenter          DataCenter `json:"data_center"`
	Description         string     `json:"description"`
	Disks               []Disk     `json:"disks"`
	ID                  string     `json:"id"`
	Name                string     `json:"name"`
	StorageHardLimitPCT string     `json:"storage_hard_limit_pct"`
	StorageSoftLimitPCT string     `json:"storage_soft_limit_pct"`
	Users               []User     `json:"users"`
	VMs                 []VM       `json:"v_ms"`
	// Links
	Permissions        []*Permission        `json:"permissions"`
	QuotaClusterLimits []*QuotaClusterLimit `json:"quota_cluster_limits"`
	QuotaStorageLimits []*QuotaStorageLimit `json:"quota_storage_limits"`
}

// QuotaClusterLimit see: https://engine.domain.tld/ovirt-engine/apidoc/#types/quota_cluster_limit
type QuotaClusterLimit struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	MemoryLimit string `json:"memory_limit"`
	MemoryUsage string `json:"memory_usage"`
	Name        string `json:"name"`
	VCPULimit   string `json:"vcpu_limit"`
	VCPUUsage   string `json:"vcpu_usage"`
	// Links
	Cluster *Cluster `json:"cluster"`
	Quota   *Quota   `json:"quota"`
}

// QuotaStorageLimit see: https://engine.domain.tld/ovirt-engine/apidoc/#types/quota_storage_limit
type QuotaStorageLimit struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Limit       string `json:"limit"`
	Name        string `json:"name"`
	Usage       string `json:"usage"`
	// Links
	Quota         *Quota         `json:"quota"`
	StorageDomain *StorageDomain `json:"storage_domain"`
}

// Range see: https://engine.domain.tld/ovirt-engine/apidoc/#types/range
type Range struct {
	// Attributes
	From string `json:"from"`
	To   string `json:"to"`
}

// ReportedConfiguration see: https://engine.domain.tld/ovirt-engine/apidoc/#types/reported_configuration
type ReportedConfiguration struct {
	// Attributes
	ActualValue   string `json:"actual_value"`
	ExpectedValue string `json:"expected_value"`
	InSync        string `json:"in_sync"`
	Name          string `json:"name"`
}

// ReportedDevice see: https://engine.domain.tld/ovirt-engine/apidoc/#types/reported_device
type ReportedDevice struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	IPs         struct {
		IP []IP `json:"ip"`
	} `json:"ips"`
	MAC  MAC    `json:"mac"`
	Name string `json:"name"`
	Type string `json:"type"`
	// Links
	VM *VM `json:"vm"`
}

// RNGDevice see: https://engine.domain.tld/ovirt-engine/apidoc/#types/rng_device
type RNGDevice struct {
	// Attributes
	Rate struct {
		Bytes  string `json:"bytes"`
		Period string `json:"period"`
	} `json:"rate"`
	Source string `json:"source"`
}

// Role see: https://engine.domain.tld/ovirt-engine/apidoc/#types/role
type Role struct {
	// Attributes
	Administrative string `json:"administrative"`
	Comment        string `json:"comment"`
	Description    string `json:"description"`
	ID             string `json:"id"`
	Mutable        string `json:"mutable"`
	Name           string `json:"name"`
}

// SchedulingPolicy see: https://engine.domain.tld/ovirt-engine/apidoc/#types/scheduling_policy
type SchedulingPolicy struct {
	// Attributes
	Comment       string     `json:"comment"`
	DefaultPolicy string     `json:"default_policy"`
	Description   string     `json:"description"`
	ID            string     `json:"id"`
	Locked        string     `json:"locked"`
	Name          string     `json:"name"`
	Properties    []Property `json:"properties"`
	// Links
	Balances []*Balance `json:"balances"`
	Filters  []*Filter  `json:"filters"`
	Weight   []*Weight  `json:"weight"`
}

// SchedulingPolicyUnit see: https://engine.domain.tld/ovirt-engine/apidoc/#types/scheduling_policy_unit
type SchedulingPolicyUnit struct {
	// Attributes
	Comment     string     `json:"comment"`
	Description string     `json:"description"`
	Enabled     string     `json:"enabled"`
	ID          string     `json:"id"`
	Internal    string     `json:"internal"`
	Name        string     `json:"name"`
	Properties  []Property `json:"properties"`
	Type        string     `json:"type"`
}

// SerialNumber see: https://engine.domain.tld/ovirt-engine/apidoc/#types/serial_number
type SerialNumber struct {
	// Attributes
	Policy string `json:"policy"`
	Value  string `json:"value"`
}

// Session see: https://engine.domain.tld/ovirt-engine/apidoc/#types/session
type Session struct {
	// Attributes
	Comment     string `json:"comment"`
	ConsoleUser string `json:"console_user"`
	Description string `json:"description"`
	ID          string `json:"id"`
	IP          IP     `json:"ip"`
	Name        string `json:"name"`
	Protocol    string `json:"protocol"`
	// Links
	User *User `json:"user"`
	VM   *VM   `json:"vm"`
}

// SkipIfConnectivityBroken see: https://engine.domain.tld/ovirt-engine/apidoc/#types/skip_if_connectivity_broken
type SkipIfConnectivityBroken struct {
	// Attributes
	Enabled   string `json:"enabled"`
	Threshold string `json:"threshold"`
}

// SkipIfSDActive see: https://engine.domain.tld/ovirt-engine/apidoc/#types/skip_if_sd_active
type SkipIfSDActive struct {
	// Attributes
	Enabled string `json:"enabled"`
}

// Snapshot see: https://engine.domain.tld/ovirt-engine/apidoc/#types/snapshot
type Snapshot struct {
	// Attributes
	BIOS                       BIOS                 `json:"bios"`
	Comment                    string               `json:"comment"`
	Console                    Console              `json:"console"`
	CPU                        CPU                  `json:"cpu"`
	CPUShares                  string               `json:"cpu_shares"`
	CreationTime               int                  `json:"creation_time"`
	CustomCompatabilityVersion Version              `json:"custom_compatability_version"`
	CustomCPUModel             string               `json:"custom_cpu_model"`
	CustomEmulatedMachine      string               `json:"custom_emulated_machine"`
	CustomProperties           []CustomProperty     `json:"custom_properties"`
	Date                       int                  `json:"date"`
	DeleteProtected            string               `json:"delete_protected"`
	Description                string               `json:"description"`
	Display                    Display              `json:"display"`
	Domain                     Domain               `json:"domain"`
	FQDN                       string               `json:"fqdn"`
	GuestOperatingSystem       GuestOperatingSystem `json:"guest_operating_system"`
	GuestTimeZone              TimeZone             `json:"guest_time_zone"`
	HighAvailability           HighAvailability     `json:"high_availability"`
	ID                         string               `json:"id"`
	Initialization             Initialization       `json:"initialization"`
	IO                         IO                   `json:"io"`
	LargeIcon                  Icon                 `json:"large_icon"`
	Lease                      StorageDomainLease   `json:"lease"`
	Memory                     string               `json:"memory"`
	MemoryPolicy               MemoryPolicy         `json:"memory_policy"`
	Migration                  MigrationOptions     `json:"migration"`
	MigrationDowntime          string               `json:"migration_downtime"`
	Name                       string               `json:"name"`
	NextRunConfigurationExists string               `json:"next_run_configuration_exists"`
	NumaTuneZone               string               `json:"numa_tune_zone"`
	Origin                     string               `json:"origin"`
	OS                         OperatingSystem      `json:"os"`
	Payloads                   []Payload            `json:"payloads"`
	PersistMemorystate         string               `json:"persist_memorystate"`
	PlacementPolicy            VMPlacementPolicy    `json:"placement_policy"`
	RNGDevice                  RNGDevice            `json:"rng_device"`
	RunOnce                    string               `json:"run_once"`
	SerialNumber               SerialNumber         `json:"serial_number"`
	SmallIcon                  Icon                 `json:"small_icon"`
	SnapshotStatus             string               `json:"snapshot_status"`
	SoundcardEnabled           string               `json:"soundcard_enabled"`
	SSO                        SSO                  `json:"sso"`
	StartPaused                string               `json:"start_paused"`
	StartTime                  int                  `json:"start_time"`
	Stateless                  string               `json:"stateless"`
	Status                     string               `json:"status"`
	StatusDetail               string               `json:"status_detail"`
	StopReason                 string               `json:"stop_reason"`
	StopTime                   int                  `json:"stop_time"`
	TimeZone                   TimeZone             `json:"time_zone"`
	TunnelMigration            string               `json:"tunnel_migration"`
	Type                       string               `json:"type"`
	USB                        USB                  `json:"usb"`
	UseLatestTemplateVersion   string               `json:"use_latest_template_version"`
	VirtioSCSI                 VirtioSCSI           `json:"virtio_scsi"`
	// Links
	AffinityLabels       []*AffinityLabel      `json:"affinity_labels"`
	Applications         []*Application        `json:"applications"`
	CDROMs               []*CDROM              `json:"cdroms"`
	Cluster              *Cluster              `json:"cluster"`
	CPUProfile           *CPUProfile           `json:"cpu_profile"`
	DiskAttachments      []*DiskAttachment     `json:"disk_attachments"`
	ExternalHostProvider *ExternalHostProvider `json:"external_host_provider"`
	Floppies             []*Floppy             `json:"floppies"`
	GraphicsConsoles     []*GraphicsConsole    `json:"graphics_consoles"`
	Host                 *Host                 `json:"host"`
	HostDevices          []*HostDevice         `json:"host_devices"`
	InstanceType         *InstanceType         `json:"instance_type"`
	KatelloErrata        []*KatelloErratum     `json:"katello_errata"`
	NICs                 []*NIC                `json:"nics"`
	NumaNodes            []*NumaNode           `json:"numa_nodes"`
	OriginalTemplate     *Template             `json:"original_template"`
	Permissions          []*Permission         `json:"permissions"`
	Quota                *Quota                `json:"quota"`
	ReportedDeivces      []*ReportedDevice     `json:"reported_deivces"`
	Sessions             []*Session            `json:"sessions"`
	Snapshots            []*Snapshot           `json:"snapshots"`
	Statistics           []*Statistic          `json:"statistics"`
	StorageDomain        *StorageDomain        `json:"storage_domain"`
	Tags                 []*Tag                `json:"tags"`
	Template             *Template             `json:"template"`
	VM                   *VM                   `json:"vm"`
	VMPool               *VMPool               `json:"vm_pool"`
	Watchdogs            []*Watchdog           `json:"watchdogs"`
}

// SpecialObject see: https://engine.domain.tld/ovirt-engine/apidoc/#types/special_object
type SpecialObject struct {
	// Attributes
	Href string `json:"href"`
	ID   string `json:"id"`
}

// SpecialObjects see: https://engine.domain.tld/ovirt-engine/apidoc/#types/special_objects
type SpecialObjects struct {
	// Attributes
	BlankTemplate Template `json:"blank_template"`
	RootTag       Tag      `json:"root_tag"`
}

// Statistic see: https://engine.domain.tld/ovirt-engine/apidoc/#types/statistic
type Statistic struct {
	// Attributes
	Comment     string  `json:"comment"`
	Description string  `json:"description"`
	ID          string  `json:"id"`
	Kind        string  `json:"kind"`
	Name        string  `json:"name"`
	Type        string  `json:"type"`
	Unit        string  `json:"unit"`
	Values      []Value `json:"values"`
	// Links
	Brick         *GlusterBrick  `json:"brick"`
	Disk          *Disk          `json:"disk"`
	GlusterVolume *GlusterVolume `json:"gluster_volume"`
	Host          *Host          `json:"host"`
	HostNIC       *HostNIC       `json:"host_nic"`
	HostNumaNode  *NumaNode      `json:"host_numa_node"`
	NIC           *NIC           `json:"nic"`
	Step          *Step          `json:"step"`
	VM            *VM            `json:"vm"`
}

// StorageConnection see: https://engine.domain.tld/ovirt-engine/apidoc/#types/storage_connection
type StorageConnection struct {
	// Attributes
	Address      string `json:"address"`
	Comment      string `json:"comment"`
	Description  string `json:"description"`
	ID           string `json:"id"`
	MountOptions string `json:"mount_options"`
	Name         string `json:"name"`
	NFSRetrans   string `json:"nfs_retrans"`
	NFSTimeo     string `json:"nfs_timeo"`
	NFSVersion   string `json:"nfs_version"`
	Password     string `json:"password"`
	Path         string `json:"path"`
	Port         string `json:"port"`
	Portal       string `json:"portal"`
	Target       string `json:"target"`
	Type         string `json:"type"`
	Username     string `json:"username"`
	VFSType      string `json:"vfs_type"`
	// Links
	Host *Host `json:"host"`
}

// StorageConnectionExtension see: https://engine.domain.tld/ovirt-engine/apidoc/#types/storage_connection_extension
type StorageConnectionExtension struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	Password    string `json:"password"`
	Target      string `json:"target"`
	Username    string `json:"username"`
	// Links
	Host *Host `json:"host"`
}

// StorageDomainLease see: https://engine.domain.tld/ovirt-engine/apidoc/#types/storage_domain_lease
type StorageDomainLease struct {
	// Attributes
	StorageDomain StorageDomain `json:"storage_domain"`
}

// SSH see: https://engine.domain.tld/ovirt-engine/apidoc/#types/ssh
type SSH struct {
	// Attributes
	AuthenticationMethod string `json:"authentication_method"`
	Comment              string `json:"comment"`
	Description          string `json:"description"`
	Fingerprint          string `json:"fingerprint"`
	ID                   string `json:"id"`
	Name                 string `json:"name"`
	Port                 string `json:"port"`
	User                 User   `json:"user"`
}

// SSHPublicKey see: https://engine.domain.tld/ovirt-engine/apidoc/#types/ssh_public_key
type SSHPublicKey struct {
	// Attributes
	Comment     string `json:"comment"`
	Content     string `json:"content"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	User *User `json:"user"`
}

// SSO see: https://engine.domain.tld/ovirt-engine/apidoc/#types/sso
type SSO struct {
	// Attributes
	Methods struct {
		Method []struct {
			ID string `json:"id"`
		}
	} `json:"methods"`
}

// Step see: https://engine.domain.tld/ovirt-engine/apidoc/#types/step
type Step struct {
	// Attributes
	Commnet      string `json:"commnet"`
	Description  string `json:"description"`
	EndTime      int    `json:"end_time"`
	External     string `json:"external"`
	ExternalType string `json:"external_type"`
	ID           string `json:"id"`
	Name         string `json:"name"`
	Number       string `json:"number"`
	Progress     string `json:"progress"`
	StartTime    int    `json:"start_time"`
	Status       string `json:"status"`
	Type         string `json:"type"`
	// Links
	ExecutionHost *Host        `json:"execution_host"`
	Job           *Job         `json:"job"`
	ParentStep    *Step        `json:"parent_step"`
	Statistics    []*Statistic `json:"statistics"`
}

// StorageDomain see: https://engine.domain.tld/ovirt-engine/apidoc/#types/storage_domain
type StorageDomain struct {
	// Attributes
	Avilable                   string      `json:"avilable"`
	Comment                    string      `json:"comment"`
	Committed                  string      `json:"committed"`
	CriticalSpaceActionBlocker string      `json:"critical_space_action_blocker"`
	Description                string      `json:"description"`
	DiscardAfterDelete         string      `json:"discard_after_delete"`
	ExternalStatus             string      `json:"external_status"`
	ID                         string      `json:"id"`
	Import                     string      `json:"import"`
	Master                     string      `json:"master"`
	Name                       string      `json:"name"`
	Status                     string      `json:"status"`
	Storage                    HostStorage `json:"storage"`
	StorageFormat              string      `json:"storage_format"`
	SupportsDiscard            string      `json:"supports_discard"`
	SupportsDiscardZeroesData  string      `json:"supports_discard_zeroes_data"`
	Type                       string      `json:"type"`
	Used                       string      `json:"used"`
	WarningLowSpaceIndicator   string      `json:"warning_low_space_indicator"`
	WipeAfterDelete            string      `json:"wipe_after_delete"`
	// Links
	DataCenter         *DataCenter          `json:"data_center"`
	DataCenters        []*DataCenter        `json:"data_centers"`
	DiskProfiles       []*DiskProfile       `json:"disk_profiles"`
	DiskSnapshot       []*DiskSnapshot      `json:"disk_snapshot"`
	Disks              []*Disk              `json:"disks"`
	Files              []*File              `json:"files"`
	Host               *Host                `json:"host"`
	Images             []*Image             `json:"images"`
	Permissions        []*Permission        `json:"permissions"`
	StorageConnections []*StorageConnection `json:"storage_connections"`
	Templates          []*Template          `json:"templates"`
	VMs                []*VM                `json:"vms"`
}

// Tag see: https://engine.domain.tld/ovirt-engine/apidoc/#types/tag
type Tag struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	Group    *Group    `json:"group"`
	Host     *Host     `json:"host"`
	Parent   *Tag      `json:"parent"`
	Template *Template `json:"template"`
	User     *User     `json:"user"`
	VM       *VM       `json:"vm"`
}

// Template see: https://engine.domain.tld/ovirt-engine/apidoc/#types/template
type Template struct {
	// Attributes
	BIOS                       BIOS               `json:"bios"`
	Comment                    string             `json:"comment"`
	Console                    Console            `json:"console"`
	CPU                        CPU                `json:"cpu"`
	CPUShares                  string             `json:"cpu_shares"`
	CreationTime               int                `json:"creation_time"`
	CustomCompatabilityVersion Version            `json:"custom_compatability_version"`
	CustomCPUModel             string             `json:"custom_cpu_model"`
	CustomEmulatedMachine      string             `json:"custom_emulated_machine"`
	CustomProperties           []CustomProperty   `json:"custom_properties"`
	DeleteProtected            string             `json:"delete_protected"`
	Description                string             `json:"description"`
	Display                    Display            `json:"display"`
	Domain                     Domain             `json:"domain"`
	HighAvailability           HighAvailability   `json:"high_availability"`
	ID                         string             `json:"id"`
	Initialization             Initialization     `json:"initialization"`
	IO                         IO                 `json:"io"`
	LargeIcon                  Icon               `json:"large_icon"`
	Lease                      StorageDomainLease `json:"lease"`
	Memory                     int                `json:"memory"`
	MemoryPolicy               MemoryPolicy       `json:"memory_policy"`
	Migration                  MigrationOptions   `json:"migration"`
	MigrationDowntime          string             `json:"migration_downtime"`
	Name                       string             `json:"name"`
	Origin                     string             `json:"origin"`
	OS                         OperatingSystem    `json:"os"`
	RNGDevice                  RNGDevice          `json:"rng_device"`
	SerialNumber               SerialNumber       `json:"serial_number"`
	SmallIcon                  Icon               `json:"small_icon"`
	SoundcardEnabled           string             `json:"soundcard_enabled"`
	SSO                        SSO                `json:"sso"`
	StartPaused                string             `json:"start_paused"`
	Stateless                  string             `json:"stateless"`
	Status                     string             `json:"status"`
	TimeZone                   TimeZone           `json:"time_zone"`
	TunnelMigration            string             `json:"tunnel_migration"`
	Type                       string             `json:"type"`
	USB                        USB                `json:"usb"`
	Version                    TemplateVersion    `json:"version"`
	VirtioSCSI                 VirtioSCSI         `json:"virtio_scsi"`
	VM                         VM                 `json:"vm"`
	// Links
	CDROMs           []*CDROM           `json:"cdroms"`
	Cluster          *Cluster           `json:"cluster"`
	CPUProfile       *CPUProfile        `json:"cpu_profile"`
	DiskAttachments  []*DiskAttachment  `json:"disk_attachments"`
	GraphicsConsoles []*GraphicsConsole `json:"graphics_consoles"`
	NICs             []*NIC             `json:"nics"`
	Permissions      []*Permission      `json:"permissions"`
	Quota            *Quota             `json:"quota"`
	StorageDomain    *StorageDomain     `json:"storage_domain"`
	Tags             []*Tag             `json:"tags"`
	Watchdogs        []*Watchdog        `json:"watchdogs"`
}

// TemplateVersion see: https://engine.domain.tld/ovirt-engine/apidoc/#types/template_version
type TemplateVersion struct {
	// Attributes
	VersionName   string `json:"version_name"`
	VersionNumber string `json:"version_number"`
	// Links
	BaseTemplate *Template `json:"base_template"`
}

// TimeZone see: https://engine.domain.tld/ovirt-engine/apidoc/#types/time_zone
type TimeZone struct {
	// Attributes
	Name      string `json:"name"`
	UTCOffset string `json:"utc_offset"`
}

// UnmanagedNetwork see: https://engine.domain.tld/ovirt-engine/apidoc/#types/unmanged_network
type UnmanagedNetwork struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	Host    *Host    `json:"host"`
	HostNIC *HostNIC `json:"host_nic"`
}

// USB see: https://engine.domain.tld/ovirt-engine/apidoc/#types/usb
type USB struct {
	// Attributes
	Enabled string `json:"enabled"`
	Type    string `json:"type"`
}

// User see: https://engine.domain.tld/ovirt-engine/apidoc/#types/user
type User struct {
	// Attributes
	Comment       string `json:"comment"`
	Department    string `json:"department"`
	Description   string `json:"description"`
	DomainEntryID string `json:"domain_entry_id"`
	Email         string `json:"email"`
	ID            string `json:"id"`
	LastName      string `json:"last_name"`
	LoggedIn      string `json:"logged_in"`
	Name          string `json:"name"`
	Namespace     string `json:"namespace"`
	Password      string `json:"password"`
	Principal     string `json:"principal"`
	UserName      string `json:"user_name"`
	// Links
	Domain        *Domain         `json:"domain"`
	Groups        []*Group        `json:"groups"`
	Permissions   []*Permission   `json:"permissions"`
	Roles         []*Role         `json:"roles"`
	SSHPublicKeys []*SSHPublicKey `json:"ssh_public_keys"`
	Tags          []*Tag          `json:"tags"`
}

// Value see: https://engine.domain.tld/ovirt-engine/apidoc/#types/value
type Value struct {
	// Attributes
	Datum  string `json:"datum"`
	Detail string `json:"detail"`
}

// Vendor see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vendor
type Vendor struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Name        string `json:"name"`
}

// Version see: https://engine.domain.tld/ovirt-engine/apidoc/#types/version
type Version struct {
	// Attributes
	Build       string `json:"build"`
	FullVersion string `json:"full_version"`
	Major       string `json:"major"`
	Minor       string `json:"minor"`
	Revision    string `json:"revision,omitempty"`
}

// VirtioSCSI see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vertio_scsi
type VirtioSCSI struct {
	// Attributes
	Enabled string `json:"enabled"`
}

// VLAN see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vlan
type VLAN struct {
	ID string `json:"id"`
}

// VM see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vm
type VM struct {
	// Attributes
	BIOS                       BIOS    `json:"bios"`
	Comment                    string  `json:"comment"`
	Console                    Console `json:"console"`
	CPU                        CPU     `json:"cpu"`
	CPUShares                  string  `json:"cpu_shares"`
	CreationTime               int     `json:"creation_time"`
	CustomCompatabilityVersion struct {
		Build       string `json:"build"`
		Comment     string `json:"comment"`
		Description string `json:"description"`
		FullVersion string `json:"full_version"`
		ID          string `json:"id"`
		Major       string `json:"major"`
		Minor       string `json:"minor"`
		Revision    string `json:"revision"`
	} `json:"custom_compatability_version"`
	CustomCPUModel        string               `json:"custom_cpu_model"`
	CustomEmulatedMachine string               `json:"custom_emulated_machine"`
	CustomProperties      []CustomProperty     `json:"custom_properties"`
	DeleteProtected       string               `json:"delete_protected"`
	Description           string               `json:"description"`
	Display               Display              `json:"display"`
	Domain                Domain               `json:"domain"`
	FQDN                  string               `json:"fqdn"`
	GuestOperatingSystem  GuestOperatingSystem `json:"guest_operating_system"`
	GuestTimeZone         struct {
		Name      string `json:"name"`
		UtcOffset string `json:"utc_offset"`
	} `json:"guest_time_zone,omitempty"`
	HighAvailability           HighAvailability   `json:"high_availability"`
	ID                         string             `json:"id"`
	Initialization             Initialization     `json:"initialization"`
	IO                         IO                 `json:"io"`
	LargeIcon                  Icon               `json:"large_icon"`
	Lease                      StorageDomainLease `json:"lease"`
	Memory                     int                `json:"memory"`
	MemoryPolicy               MemoryPolicy       `json:"memory_policy"`
	Migration                  MigrationOptions   `json:"migration"`
	MigrationDowntime          string             `json:"migration_downtime"`
	Name                       string             `json:"name"`
	NextRunConfigurationExists string             `json:"next_run_configuration_exists"`
	NumaTuneZone               string             `json:"numa_tune_zone"`
	Origin                     string             `json:"origin"`
	OS                         OperatingSystem    `json:"os"`
	Payloads                   []Payload          `json:"payloads"`
	PlacementPolicy            struct {
		Affinity string `json:"affinity"`
	} `json:"placement_policy"`
	RNGDevice                RNGDevice    `json:"rng_device"`
	RunOnce                  string       `json:"run_once"`
	SerialNumber             SerialNumber `json:"serial_number"`
	SmallIcon                Icon         `json:"small_icon"`
	SoundcardEnabled         string       `json:"soundcard_enabled"`
	SSO                      SSO          `json:"sso"`
	StartPaused              string       `json:"start_paused"`
	StartTime                int          `json:"start_time"`
	Stateless                string       `json:"stateless"`
	Status                   string       `json:"status"`
	StatusDetail             string       `json:"status_detail"`
	StopReason               string       `json:"stop_reason"`
	StopTime                 int          `json:"stop_time"`
	TimeZone                 TimeZone     `json:"time_zone"`
	TunnelMigration          string       `json:"tunnel_migration"`
	Type                     string       `json:"type"`
	USB                      USB          `json:"usb"`
	UseLatestTemplateVersion string       `json:"use_latest_template_version"`
	VirtioSCSI               VirtioSCSI   `json:"virtio_scsi"`
	// Links
	AffinityLabels       []*AffinityLabel      `json:"affinity_labels"`
	Applications         []*Application        `json:"applications"`
	CDROMs               []*CDROM              `json:"cdroms"`
	Cluster              *Cluster              `json:"cluster"`
	CPUProfile           *CPUProfile           `json:"cpu_profile"`
	DiskAttachments      []*DiskAttachment     `json:"disk_attachments"`
	ExternalHostProvider *ExternalHostProvider `json:"external_host_provider"`
	Floppies             []*Floppy             `json:"floppies"`
	GraphicsConsoles     []*GraphicsConsole    `json:"graphics_consoles"`
	Host                 *Host                 `json:"host"`
	HostDevices          []*HostDevice         `json:"host_devices"`
	InstanceType         *InstanceType         `json:"instance_type"`
	KatelloErrata        []*KatelloErratum     `json:"katello_errata"`
	NICs                 []*NIC                `json:"nics"`
	NumaNodes            []*NumaNode           `json:"numa_nodes"`
	OriginalTemplate     *Template             `json:"original_template"`
	Permissions          []*Permission         `json:"permissions"`
	Quota                *Quota                `json:"quota"`
	ReportedDeivces      []*ReportedDevice     `json:"reported_deivces"`
	Sessions             []*Session            `json:"sessions"`
	Snapshots            []*Snapshot           `json:"snapshots"`
	Statistics           []*Statistic          `json:"statistics"`
	StorageDomain        *StorageDomain        `json:"storage_domain"`
	Tags                 []*Tag                `json:"tags"`
	Template             *Template             `json:"template"`
	VMPool               *VMPool               `json:"vm_pool"`
	Watchdogs            []*Watchdog           `json:"watchdogs"`
}

// VMPlacementPolicy see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vm_placement_policy
type VMPlacementPolicy struct {
	// Attributes
	Affinity string `json:"affinity"`
	// Links
	Hosts []*Host `json:"hosts"`
}

// VMPool see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vm_pool
type VMPool struct {
	// Attributes
	AutoStorageSelect        string    `json:"auto_storage_select"`
	Comment                  string    `json:"comment"`
	Description              string    `json:"description"`
	Display                  Display   `json:"display"`
	ID                       string    `json:"id"`
	MaxUserVMs               string    `json:"max_user_vms"`
	Name                     string    `json:"name"`
	PrestartedVMs            string    `json:"prestarted_vms"`
	RNGDevice                RNGDevice `json:"rng_device"`
	Size                     string    `json:"size"`
	SoundcardEnabled         string    `json:"soundcard_enabled"`
	Stateful                 string    `json:"stateful"`
	Type                     string    `json:"type"`
	UseLatestTemplateVersion string    `json:"use_latest_template_version"`
	// Links
	Cluster      *Cluster      `json:"cluster"`
	InstanceType *InstanceType `json:"instance_type"`
	Permissions  []*Permission `json:"permissions"`
	Template     *Template     `json:"template"`
	VM           *VM           `json:"vm"`
}

// VNICPassThrough see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vnic_pass_through
type VNICPassThrough struct {
	// Attributes
	Mode string `json:"mode"`
}

// VNICProfile see: https://engine.domain.tld/ovirt-engine/apidoc/#types/vnic_profile
type VNICProfile struct {
	// Attributes
	Comment          string           `json:"comment"`
	CustomProperties []CustomProperty `json:"custom_properties"`
	Description      string           `json:"description"`
	ID               string           `json:"id"`
	Migratable       string           `json:"migratable"`
	Name             string           `json:"name"`
	PassThrough      VNICPassThrough  `json:"pass_through"`
	PortMirroring    string           `json:"port_mirroring"`
	// Links
	Network       *Network       `json:"network"`
	NetworkFilter *NetworkFilter `json:"network_filter"`
	Permissions   []*Permission  `json:"permissions"`
	QOS           *QOS           `json:"qos"`
}

// VolumeGroup see: https://engine.domain.tld/ovirt-engine/apidoc/#types/volume_group
type VolumeGroup struct {
	// Attributes
	ID           string      `json:"id"`
	LogicalUnits LogicalUnit `json:"logical_units"`
	Name         string      `json:"name"`
}

// Watchdog see: https://engine.domain.tld/ovirt-engine/apidoc/#types/watchdog
type Watchdog struct {
	// Attributes
	Action      string `json:"action"`
	Comment     string `json:"comment"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	// Links
	InstanceType *InstanceType `json:"instance_type"`
	Template     *Template     `json:"template"`
	VM           *VM           `json:"vm"`
	VMs          []*VM         `json:"vms"`
}

// Weight see: https://engine.domain.tld/ovirt-engine/apidoc/#types/weight
type Weight struct {
	// Attributes
	Comment     string `json:"comment"`
	Description string `json:"description"`
	Factor      string `json:"factor"`
	ID          string `json:"id"`
	Name        string `json:"name"`
	// Links
	SchedulingPolicy     *SchedulingPolicy     `json:"scheduling_policy"`
	SchedulingPolicyUnit *SchedulingPolicyUnit `json:"scheduling_policy_unit"`
}
