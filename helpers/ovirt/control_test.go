package ovirt

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/gitlab-ci-multi-runner/helpers"
)

const ovrtURL = "https://ovirt.gitlab.com"
const ovrtCAFile = "/etc/gitlab-runner/ovirt.pem"
const ovrtCluster = "Cluster"
const ovrtTemplateName = "Template"

var ovrt = Ovirt{
	Username: "username@internal",
	Password: "password",
	Timeout:  300,
}

//------------------------------------------------------------------------------
// Unit Tests
//------------------------------------------------------------------------------

func TestOvirtControlID(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		type ReqJSON struct {
			VM [1]VM `json:"vm"`
		}
		var reqJSON ReqJSON
		reqJSON.VM[0].Name = "Test"
		reqJSON.VM[0].ID = "123456"
		bytes, _ := json.Marshal(reqJSON)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	id, err := ovrt.ID("Test")
	assert.NoError(t, err)
	assert.Contains(t, id, "123456")
}

func TestOvirtControlVM(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var vm VM
		vm.Name = "Test"
		vm.ID = "123456"
		bytes, _ := json.Marshal(vm)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	var vm VM
	err := ovrt.VM(&vm, "123456")
	assert.NoError(t, err)
	assert.Contains(t, vm.Name, "Test")
}

func TestOvirtControlNICID(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		type ReqJSON struct {
			NIC [1]NIC `json:"nic"`
		}
		var reqJSON ReqJSON
		reqJSON.NIC[0].Interface = "Test"
		reqJSON.NIC[0].ID = "654321"
		bytes, _ := json.Marshal(reqJSON)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	id, err := ovrt.NICID("123456", "Test")
	assert.NoError(t, err)
	assert.Contains(t, id, "654321")
}

func TestOvirtControlNIC(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var nic NIC
		nic.Name = "Test"
		nic.ID = "654321"
		bytes, _ := json.Marshal(nic)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	var nic NIC
	err := ovrt.NIC(&nic, "123456", "654321")
	assert.NoError(t, err)
	assert.Contains(t, nic.Name, "Test")
}

func TestOvirtControlSnapshotID(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		type ReqJSON struct {
			Snapshot [1]Snapshot `json:"snapshot"`
		}
		var reqJSON ReqJSON
		reqJSON.Snapshot[0].Description = "Test"
		reqJSON.Snapshot[0].ID = "654321"
		bytes, _ := json.Marshal(reqJSON)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	id, err := ovrt.SnapshotID("123456", "Test")
	assert.NoError(t, err)
	assert.Contains(t, id, "654321")
}

func TestOvirtControlSnapshot(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var snapshot Snapshot
		snapshot.Description = "Test"
		snapshot.ID = "654321"
		bytes, _ := json.Marshal(snapshot)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	var snapshot Snapshot
	err := ovrt.Snapshot(&snapshot, "123456", "654321")
	assert.NoError(t, err)
	assert.Contains(t, snapshot.Description, "Test")
}

func TestOvirtControlStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var vm VM
		vm.Name = "Test"
		vm.ID = "123456"
		vm.Status = "up"
		bytes, _ := json.Marshal(vm)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	status, err := ovrt.Status("123456")
	assert.NoError(t, err)
	assert.Contains(t, status, "up")
}

func TestOvirtControlWaitForStatus(t *testing.T) {
	flag := false
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var vm VM
		vm.Name = "Test"
		vm.ID = "123456"
		if flag {
			vm.Status = "up"
		} else {
			vm.Status = "down"
		}
		bytes, _ := json.Marshal(vm)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
		flag = true
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	err := ovrt.WaitForStatus("123456", []string{"up"}, 300)
	assert.NoError(t, err)
}

func TestOvirtControlSnapshotStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var snapshot Snapshot
		snapshot.ID = "654321"
		snapshot.SnapshotStatus = "up"
		bytes, _ := json.Marshal(snapshot)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	status, err := ovrt.SnapshotStatus("123456", "654321")
	assert.NoError(t, err)
	assert.Contains(t, status, "up")
}

func TestOvirtControlWaitForSnapshotStatus(t *testing.T) {
	flag := false
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var snapshot Snapshot
		snapshot.ID = "654321"
		if flag {
			snapshot.SnapshotStatus = "up"
		} else {
			snapshot.SnapshotStatus = "down"
		}
		bytes, _ := json.Marshal(snapshot)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
		flag = true
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	err := ovrt.WaitForSnapshotStatus("123456", "654321", "up", 300)
	assert.NoError(t, err)
}

func TestOvirtControlCreateFromTemplate(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var vm VM
		vm.Name = "Test"
		vm.ID = "123456"
		vm.Status = "down"
		bytes, _ := json.Marshal(vm)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "POST" {
			w.WriteHeader(http.StatusAccepted)
		}
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	id, err := ovrt.CreateFromTemplate("Test", "Template", "Cluster")
	assert.NoError(t, err)
	assert.Contains(t, id, "123456")
}

func TestOvirtControlCreateFromSnapshot(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var vm VM
		vm.Name = "Test"
		vm.ID = "123456"
		vm.Status = "down"
		bytes, _ := json.Marshal(vm)
		if strings.Contains(r.URL.String(), "/snapshots") {
			type ReqJSON struct {
				Snapshot [1]Snapshot `json:"snapshot"`
			}
			var reqJSON ReqJSON
			reqJSON.Snapshot[0].Description = "Snapshot"
			reqJSON.Snapshot[0].ID = "654321"
			bytes, _ = json.Marshal(reqJSON)
		}
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "POST" {
			w.WriteHeader(http.StatusAccepted)
		}
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	id, err := ovrt.CreateFromSnapshot("Test", "654321", "Snapshot", "Cluster")
	assert.NoError(t, err)
	assert.Contains(t, id, "123456")
}

func TestOvirtControlDelete(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		type ReqJSON struct {
			Job    Link   `json:"job"`
			Status string `json:"status"`
		}
		var reqJSON ReqJSON
		reqJSON.Status = "complete"
		bytes, _ := json.Marshal(reqJSON)
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	err := ovrt.Delete("123456")
	assert.NoError(t, err)
}

func TestOvirtControlStart(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var vm VM
		vm.Name = "Test"
		vm.ID = "123456"
		vm.Status = "up"
		bytes, _ := json.Marshal(vm)
		if strings.Contains(r.URL.String(), "/start") {
			type ReqJSON struct {
				Async  string `json:"async"`
				Job    Link   `json:"job"`
				Status string `json:"status"`
				VM     VM     `json:"vm"`
			}
			var reqJSON ReqJSON
			reqJSON.Status = "complete"
			bytes, _ = json.Marshal(reqJSON)
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	err := ovrt.Start("123456")
	assert.NoError(t, err)
}

func TestOvirtControlShutdown(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var vm VM
		vm.Name = "Test"
		vm.ID = "123456"
		vm.Status = "down"
		bytes, _ := json.Marshal(vm)
		if strings.Contains(r.URL.String(), "/shutdown") {
			type ReqJSON struct {
				Async  string `json:"async"`
				Job    Link   `json:"job"`
				Status string `json:"status"`
				VM     VM     `json:"vm"`
			}
			var reqJSON ReqJSON
			reqJSON.Status = "complete"
			bytes, _ = json.Marshal(reqJSON)
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	err := ovrt.Shutdown("123456")
	assert.NoError(t, err)
}

func TestOvirtControlCreateSnapshot(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var snapshot Snapshot
		snapshot.ID = "654321"
		snapshot.SnapshotStatus = "ok"
		bytes, _ := json.Marshal(snapshot)
		w.Header().Set("Content-Type", "application/json")
		if r.Method == "POST" {
			w.WriteHeader(http.StatusAccepted)
		}
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	id, err := ovrt.CreateSnapshot("123456", "Test")
	assert.NoError(t, err)
	assert.Contains(t, id, "654321")
}

func TestOvirtControlRestoreFromSnapshot(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var snapshot Snapshot
		snapshot.ID = "654321"
		snapshot.SnapshotStatus = "up"
		bytes, _ := json.Marshal(snapshot)
		if strings.Contains(r.URL.String(), "/restore") {
			type ReqJSON struct {
				Async  string `json:"async"`
				Job    Link   `json:"job"`
				Status string `json:"status"`
				VM     VM     `json:"vm"`
			}
			var reqJSON ReqJSON
			reqJSON.Status = "complete"
			bytes, _ = json.Marshal(reqJSON)
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(bytes)
	}))
	defer ts.Close()

	ovrt.Client = ovrt.HTTPClient("", 300)
	ovrt.URL = ts.URL

	err := ovrt.RestoreSnapshot("123456", "654321")
	assert.NoError(t, err)
}

//------------------------------------------------------------------------------
// Integration Tests
//------------------------------------------------------------------------------

// TODO: Due to the thin clone bug upstream, we don't test SnapshotBased

func skipIntegrationTests(t *testing.T) bool {
	ovrt.Client = ovrt.HTTPClient(ovrtCAFile, 5)
	ovrt.URL = ovrtURL
	_, err := ovrt.sendRequest("GET", "vms", "", 200)
	if err != nil {
		return true
	}
	return false
}

func TestOvirtControlTemplateBased(t *testing.T) {
	if helpers.SkipIntegrationTests(t) || skipIntegrationTests(t) {
		return
	}

	var nicID = ""
	var snapshotID = ""
	var vmID = ""

	ovrt.Client = ovrt.HTTPClient(ovrtCAFile, 300)
	ovrt.URL = ovrtURL

	vmID, err := ovrt.CreateFromTemplate("IntegrationTest", ovrtTemplateName, ovrtCluster)
	assert.NoError(t, err)
	if !assert.NotContains(t, "", vmID) {
		return
	}

	id, err := ovrt.ID("IntegrationTest")
	assert.NoError(t, err)
	if !assert.Contains(t, id, vmID) {
		return
	}

	var vm VM
	err = ovrt.VM(&vm, vmID)
	assert.NoError(t, err)
	if !assert.Contains(t, vm.Name, "IntegrationTest") {
		return
	}

	err = ovrt.WaitForStatus(vmID, []string{"up", "down"}, 300)
	if !assert.NoError(t, err) {
		return
	}

	snapshotID, err = ovrt.CreateSnapshot(vmID, "Test")
	assert.NoError(t, err)
	if !assert.NotContains(t, "", snapshotID) {
		return
	}

	id, err = ovrt.SnapshotID(vmID, "Test")
	assert.NoError(t, err)
	if !assert.Contains(t, id, snapshotID) {
		return
	}

	var snapshot Snapshot
	err = ovrt.Snapshot(&snapshot, vmID, snapshotID)
	assert.NoError(t, err)
	if !assert.Contains(t, snapshot.ID, snapshotID) {
		return
	}

	err = ovrt.Start(vmID)
	if !assert.NoError(t, err) {
		return
	}

	nicID, err = ovrt.NICID(vmID, "virtio")
	assert.NoError(t, err)
	if !assert.NotContains(t, "", nicID) {
		return
	}

	var nic NIC
	err = ovrt.NIC(&nic, vmID, nicID)
	assert.NoError(t, err)
	if !assert.Contains(t, nic.ID, nicID) {
		return
	}

	err = ovrt.Shutdown(vmID)
	if !assert.NoError(t, err) {
		return
	}

	err = ovrt.RestoreSnapshot(vmID, snapshotID)
	if !assert.NoError(t, err) {
		return
	}

	err = ovrt.Delete(vmID)
	if !assert.NoError(t, err) {
		return
	}
}
